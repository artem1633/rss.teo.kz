<?php

namespace app\models;

use Codeception\Util\Debug;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property string $done
 * @property integer $project_id
 * @property integer $object_id
 * @property string $future_plan
 * @property string $problems
 * @property string $email_dispatch
 *  Адреса электронныйх почт, на которая произведется рассылка отчета
 * @property integer $creator_id
 * @property string $creation_date
 * @property integer $post_send
 * @property integer $prev_report
 * @property array $user_list
 * @property array $imageFile[]
 * @property array $work
 * @property Project $project
 * @property Users $creator
 *
 * @property Object $object //объект по которому сделан отчет
 *
 */
class Report extends \yii\db\ActiveRecord
{
    public function __construct(array $config = [])
    {
        $this->isSentFlag = false;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public $has_works;

    public $is_send_report;

    public $isSentFlag; // Флаг сигнализирующий о том, что отчет был отправлен

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['done', 'project_id', 'user_list', 'object_id', 'begin_work'], 'required'],
            [['done', 'future_plan', 'problems', 'has_works'], 'string'],
            ['email_dispatch', 'validateMultiEmails'],
            [['begin_work'], 'default', 'value' => null],
            [['driving', 'renta_avto', 'avto'], 'integer'],
            [['driving', 'renta_avto', 'avto'], 'validateStackAvtoFields'],
            [['project_id', 'creator_id', 'post_send', 'prev_report'], 'integer'],
            [['creation_date', 'imageFiles', 'has_works'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
        ];
    }

    public function validateMultiEmails($attribute, $params, $validator)
    {
        $emails = $this->prepareEmails();

        foreach ($emails as $email) {

            $emailValidator = new \yii\validators\EmailValidator();
            if ($emailValidator->validate($email)) {
                continue;
            } else {
                $this->addError($attribute, 'Присутствуют некорректные адреса электронной почты');
            }
        }
    }

    public function validateStackAvtoFields($attribute, $params, $validator)
    {
        $labels = $this->attributeLabels();
        if (!$this->driving || !$this->renta_avto) {
            if ($this->avto == "") {
                $fields = "Поля ";
                $fields .= $labels['driving'] . " или ";
                $fields .= $labels['renta_avto'] . " и ";
                $fields .= $labels['avto'] . " ";
                $this->addError($attribute, $fields . ' должны быть заполнены совместно');
                $this->addError('avto', $fields . ' должны быть заполнены совместно');
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID Отчёта',
            'done' => 'Что сделано',
            'work' => 'Работы',
            'project_id' => 'Проект',
            'object_id' => 'Объект',
            'future_plan' => 'План на следующий день',
            'email_dispatch' => 'Список рассылки',
            'problems' => 'Проблемы',
            'creator_id' => 'Создатель',
            'creation_date' => 'Дата создания',
            'post_send' => 'Список получивших',
            'prev_report' => 'Предыдущий отчёт',
            'user_list' => 'Бригада',
            'imageFiles' => 'Приложения к отчету',
            'driving' => 'Водительские (км)',
            'renta_avto' => 'Аренда авто (км)',
            'avto' => 'Автомобиль',
            'begin_work' => 'Дата проведения работ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * Возвращает объект по которому сделан отчет
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Object::className(), ['id' => 'object_id']);
    }

    public function getWorks()
    {
        return WorkProcess::find()->where(['report_id' => $this->id])->all();

    }


    public function getAvtomobile()
    {
        return $this->hasOne(Cars::className(), ['id' => 'avto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }


    public function beforeSave($insert)
    {
        if ($this->has_works == null)
            $this->has_works = 0;

        $this->user_list = $_POST['Report']['user_list'];

        $this->user_list = implode(', ', $this->user_list);

        if ($insert) {
            $this->creator_id = Yii::$app->user->id;
        }


        if ($this->isNewRecord) {
            $users = [];

            $data = \app\models\Users::find()->where([
                'in',
                'login',
                \app\models\Project::find($this->project_id)->one()->command
            ])->all();

            foreach ($data as $user) {
                $users[] = $user->email;
            }

            $this->email_dispatch = implode(',', $users);

        }

        if ($this->email_dispatch && !$this->isNewRecord) {
            $this->email_dispatch = implode(',', $this->email_dispatch);
        }

        if ($this->convertFormat()) {
            $this->begin_work = $this->convertFormat();
        }
        if ($this->isNewRecord) {
            $this->creation_date = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $workProcesses = WorkProcess::find()->where(['report_id' => $this])->all();
        $photos = Photos::find()->where(['report_id' => $this->id])->all();

        foreach ($workProcesses as $process) {
            $process->delete();
        }

        foreach ($photos as $photo) {
            $photo->delete();
        }

        return parent::beforeDelete();
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->user_list = explode(', ', $this->user_list);
        $this->email_dispatch = explode(', ', $this->email_dispatch);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $isSending = isset($_POST['Report']['is_send_report']) ? $_POST['Report']['is_send_report'] : null;
        if ($this->imageFiles != null) {
            $images = explode(',', $this->imageFiles);
            foreach ($images as $image) {
//                $photo = new Photos(['report_id' => $this->id, 'photo' => $image]);
//                $photo->save();

                //находим фотки в базе и прикрепляем к ним идентификатор отчета
                Photos::updateReportIdByPath($this->id, $image);
            }
        }
        if ($isSending == 1) {

            $userLogins = explode(', ', $this->user_list);

            $userId = Yii::$app->user->getId();
            $sendingUser = Users::findOne(['id' => $userId]);


            $users = Users::find()->where(['in', 'login', $userLogins])->all();

            $usersEmails = ArrayHelper::map($users, 'email', 'name');

            $emails = [];

            if ($this->email_dispatch != null) {
                $emailDispatchesEmails = Users::find()->where(['in', 'email', explode(',',$this->email_dispatch)])->all();
                $emails = ArrayHelper::map($emailDispatchesEmails, 'email', 'name');

            }

            $rejectedEmails = $this->getRejectedEmails();


            $dispatchEmails = array_merge($usersEmails, $emails);

            $project = $this->project;

            if ($project != null) {
                $command = Users::findAll(['login' => $project->command]);
                $commandEmails = ArrayHelper::map($command, 'email', 'name');
                $projectManager = Users::findOne($project->pm);

                if ($projectManager != null) {
                    $dispatchEmails = array_merge($dispatchEmails, [$projectManager->email => $projectManager->name]);
                }
                $dispatchEmails = array_merge($dispatchEmails, $commandEmails);
            }
            

            $dispatchEmails = array_diff($dispatchEmails, $rejectedEmails);

            $dispatchEmails = array_unique($dispatchEmails);

            $messages = [];

            $object = Object::findOne($this->object_id);
            $objectTitle = $object != null ? $object->title : 'Объект удален';

            $project = Object::findOne($this->project_id);
            $projectTitle = $project != null ? $project->title : 'Проект удален';

            $users = Users::findAll($this->user_list);
            $usersList = implode(', ', array_column($users, 'name'));

            foreach ($dispatchEmails as $email) {
                $reportQueue = $this->getReportQueue();
                $html = '';
                foreach ($reportQueue as $report) {
                    $html .= $report->generateReportEmailHtmlBody() . '<hr>';
                }

                //прикрепляем предыдущие отчеты за 7 дней в убывающем порядке
                $html .= $this->getPreviousReports($this->id, $this->begin_work);

                $Cc = [];

                foreach ($dispatchEmails as $ccEmail => $name) {
                    if ($ccEmail != $email) {
                        array_push($Cc, $ccEmail);
                    }
                }
                if ($dispatchEmails) {
                    $message = Yii::$app->mailer->compose()
                        ->setFrom(['reports@ate.net.ru' => $this->creator->name])
                        //[email => name]
                        //FIXME: если у пользователя нет email в базе, то тут ошибка
                        ->setTo($dispatchEmails)
                        ->setSubject($this->project->title . " " . $this->creator->name . " Ежедневный отчёт")
                        ->setHtmlBody($html);

                    $photos = Photos::findAll(['report_id' => $this->id]);
                    foreach ($photos as $item) {
                        $message->attach(\Yii::getAlias('@webroot/' . $item->photo),
                            ['fileName' => $item->original_name]);
                    }

                    array_push($messages, $message);
                }
            }


            if (isset($messages[0])) {
                try {
                    Yii::$app->mailer->send($messages[0]);
                } catch (\Swift_TransportException $e) {
                    Yii::$app->session->setFlash('warning', 'Отчет не был отправлен. Ошибка: ' . $e->getMessage());
                } catch (\Exception $e) {
                    Yii::$app->session->setFlash('error', 'Отчет не был отправлен. Ошибка: ' . $e->getMessage());
                }
            }

            $this->isSentFlag = true;
        }

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * Метод возвращает строку к отчету за предыдущие отчеты пользователя за 7 дней (по умолчанию)
     * @param $not_id int идентификатор отчета, который не должен быть включен в список
     * @param $start_date string дата с которой начинается (ВКЛЮЧИТЕЛЬНО) обратный отсчет
     * @param int $day_count сколько дней назад нужны отчеты
     * @return string
     */
    private function getPreviousReports($not_id, $start_date, $day_count = 7)
    {
        $start_date = Yii::$app->formatter->asDatetime($start_date, 'Y-MM-dd');
        $user = Yii::$app->user->identity;
        /** @var Users $user */
        $reports = $user->getReports()
            ->andWhere('begin_work <= \'' . $start_date . '\'')
            ->andWhere('begin_work >= DATE_SUB( \'' . $start_date . '\' , INTERVAL ' . $day_count . ' DAY)')
            ->orderBy(['begin_work' => SORT_DESC])
            ->all();

        $html = "";
        if ($reports) {
            foreach ($reports as $report) {
                /** @var $report Report */
                if ($report->id != $not_id) {
                    $html .= $report->generateReportEmailHtmlBody();
                    $html .= "<hr>";
                }
            }
            unset($report);
        }

        return $html;
    }


    /**
     * Находит все отчетые, при ссылании на которые не сможет возникнуть замыкания
     * @return Report[] $reports
     */
    public function findFreeReports()
    {
        $pks = []; // id цепи отчетов
        $firstReferReports = Report::find()->where(['prev_report' => $this->id])->all(); // Первый отчет, который ссылается на на данный отчет в качестве предыдущего отчета


        if ($firstReferReports != null) {
            foreach ($firstReferReports as $firstReferReport) {
                array_push($pks, $firstReferReport->id);
                $this->createReportQueueOn($pks, $firstReferReport->id);
            }
            $reports = Report::find();
            foreach ($pks as $pk) {
                $reports->andFilterWhere(['!=', 'id', $pk]);
            }

            $reports->andFilterWhere(['!=', 'id', $this->id]);
            return $reports->all();
        } else {
            return Report::find()->where(['!=', 'id', $this->id])->all();
        }
    }

    /**
     * Генерирует тело HTML письма для отправки отчета на почту
     * @return string
     */
    public function generateReportEmailHtmlBody()
    {
        $sections = Section::find()->all();
        $works = Work::find()->all();
        $objects = Object::find()->all();

        $creator = Users::findOne($this->creator_id);
        $positions = ArrayHelper::map(Position::find()->all(), 'id', 'title');
        $creatorPosition = isset($positions[$creator->role]) ? $positions[$creator->role] : '<i>(Должность не определена)</i>';

        $object = Object::findOne($this->object_id);
        $objectTitle = $object != null ? $object->title : 'Объект удален';


        $project = Project::findOne($this->project_id);
        $projectTitle = $project != null ? $project->title : 'Проект удален';

        $users = Users::find()->where(['login' => $this->user_list])->all();
        $usersList = implode(', ', array_column($users, 'name'));

        $workProcessesSearch = new WorkProcessSearch();
        $processesDataProvider = $workProcessesSearch->searchByReport($this->id);

        $has_works = isset($_POST['Report']['has_works']) ? $_POST['Report']['has_works'] : null;

        return Yii::$app->controller->renderPartial('@app/views/mail/report', [
            'report' => $this,
            'objectTitle' => $objectTitle,
            'projectTitle' => $projectTitle,
            'usersList' => $usersList,
            'positions' => $positions,
            'creatorName' => $creator->name,
            'creatorEmail' => $creator->email,
            'creatorPosition' => $creatorPosition,
            'processesDataProvider' => $processesDataProvider,
            'workcounts' => Workcount::find()->where(['project_id' => $this->project_id, 'object_id' => $this->object_id])->all(),
            'objects' => $objects,
            'works' => $works,
            // 'creatorPhone' => $creator->phone,
            'creatorPhone' => $creator->phoneHtml,
            'sections' => $sections,
            'report_id' => $this->id,
            'has_works' => $has_works,
            'driving' => $this->driving,
            'renta_avto' => $this->renta_avto,
            'avto' => isset($this->avtomobile->name) ? $this->avtomobile->name : "",
        ]);
    }

    /**
     * Получаем цепь всех предыдущих отчетов, включая и этот отчет
     * @return static[] $reports
     */
    public function getReportQueue()
    {
        $pks = [];
        $this->createReportQueueOff($pks, $this->id);
        $reports = [];

        foreach ($pks as $pk) {
            $report = Report::findOne($pk);
            array_push($reports, $report);
        }

        return $reports;
    }

    /**
     * Получаем цепь из id всех предыдущих отчетов, включая и этот отчет
     * @return array $pks
     */
    public function getReportQueuePks()
    {
        $pks = [];
        $this->createReportQueueOff($pks, $this->id);
        return $pks;
    }

    /**
     * Рекурсивный метод, который составляет последовательность отчетов, которые ссылаются друг на друга.
     * Выстраивает цепь на себя, т.е. узнает какие отчеты на него ссылкаются и проводит такой же алгоритм с ними
     * @param array $pks
     * @param integer $report_id
     */
    private function createReportQueueOn(&$pks, $report_id)
    {
        $reports = Report::find()->where(['prev_report' => $report_id])->all();

        if ($reports != null) {
            foreach ($reports as $report) {
                if (in_array($report->id, $pks)) {
                    break;
                } else {
                    array_push($pks, $report->id);
                    $this->createReportQueueOn($pks, $report->id);
                }
            }
        }
    }

    /**
     * Рекурсивный метод, который составляет последовательность отчетов, которые ссылаются друг на друга.
     * Выстраивает цепь от себя, т.е. узнает на какой отчет он ссылается и проводит такой же алгоритм с ним.
     * @param array $pks
     * @param integer $report_id
     */
    private function createReportQueueOff(&$pks, $report_id)
    {
        /** @var \app\models\Report $report */
        $report = Report::find()->where(['id' => $report_id])->one();

        if ($report != null) {
            if (in_array($report->id, $pks)) {
                return;
            } else {
                array_push($pks, $report->id);
                $this->createReportQueueOff($pks, $report->prev_report);
            }
        }
    }

    protected function convertFormat()
    {
        if ($this->begin_work) {
            $date = explode('.', $this->begin_work);
            if (count($date) == 3) {
                $convertDate[0] = $date[2];
                $convertDate[1] = $date[1];
                $convertDate[2] = $date[0];
                return implode('-', $convertDate);
            }

        }
        return null;
    }

    /**
     * @return array
     */
    protected function prepareEmails()
    {
        $list = [];
        $emails = $this->email_dispatch;
        if(!is_array($this->email_dispatch)) {
            $emails = explode(',', $this->email_dispatch);
        }

        if (count($emails)) {
            foreach ($emails as $row) {
                $list[$row] = $row;
            }

        }
        return $list;
    }

    protected function getRejectedEmails()
    {
        $emailDispatches = explode(',', $this->email_dispatch);
        $userLogins = explode(',', $this->user_list);

        $userLogins = Users::find()->where(['in','login', $userLogins])->all();
        $emailDispatches = Users::find()->where(['in','email', $emailDispatches])->all();

        $sendingUser = $sendingUser = Users::findOne(['id' => $this->creator_id]);

        if (!is_array($sendingUser->recipients)) {
            $sendingUser->recipients = explode(',', $sendingUser->recipients);
        }

        $rejectedEmails = [];

        /** @var Users $userLogin */
        foreach ($userLogins as $userLogin) {
            if (!is_array($userLogin->recipients)) {
                $userLogin->recipients = explode(',', $userLogin->recipients);
            }

            if (!in_array($sendingUser->role, $userLogin->recipients)) {
                $rejectedEmails[$userLogin->email] = $userLogin->name;
            }
        }

        /** @var Users $emailDispatch */
        foreach ($emailDispatches as $emailDispatch) {
            if (!is_array($emailDispatch->recipients)) {
                $emailDispatch->recipients = explode(',', $emailDispatch->recipients);
            }

            if (!in_array($sendingUser->role, $emailDispatch->recipients)) {
                $rejectedEmails[$emailDispatch->email] = $emailDispatch->name;
            }
        }


        return $rejectedEmails;
    }

}

