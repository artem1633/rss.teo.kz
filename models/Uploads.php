<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "uploads".
 *
 * @property integer $id
 * @property string $name
 * @property string $extension
 * @property string $path
 */
class Uploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $File;
    public $proyekt_id;
    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'extension', 'path'], 'string', 'max' => 255],
            [['File'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
            [['proyekt_id'] , 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'extension' => 'Extension',
            'path' => 'Path',
        ];
    }
    public function upload()
    {
        //if ($this->validate()) {
            $this->File->saveAs('upload/' . 'excelfileimport' . '.' . $this->File->extension);
            
            return true;
       // } else {
      //      return false;
       // }
    }
}
