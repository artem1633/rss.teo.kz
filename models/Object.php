<?php

namespace app\models;

use Codeception\Util\Debug;
use Yii;

/**
 * This is the model class for table "object".
 *
 * @property integer $id
 * @property string $title
 * @property string $section
 * @property ProjectObject[] $projectObjects
 */
class Object extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'section' => 'Разделы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectObjects()
    {
        return $this->hasMany(ProjectObject::className(), ['object_id' => 'id']);
    }

    public function beforeDelete()
    {
        $constraits = ProjectObject::findAll(['object_id' => $this->id]);

        foreach ($constraits as $item)
        {
            $item->delete();
        }

        $constraits = Section::findAll(['object_id' => $this->id]);

        foreach ($constraits as $constrait)
        {
            $constrait->delete();
        }

        $constraits = ObjectSection::findAll(['object_id' => $this->id]);

        foreach ($constraits as $item)
        {
            $item->delete();
        }

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

}
