<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkProcess;

/**
 * WorkProcessSearch represents the model behind the search form about `app\models\WorkProcess`.
 */
class WorkProcessSearch extends WorkProcess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'workcount_id', 'count_daily'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkProcess::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'workcount_id' => $this->workcount_id,
            'count_daily' => $this->count_daily,
            'date' => $this->date,
        ]);

        return $dataProvider;
    }

    /**
     * Create data provider instance from workcountIds
     *
     * @param array $workcountIds
     * @param integer $report_id
     *
     * @return ActiveDataProvider
     */
    public function searchByWorkcounts($report_id, $workcountIds)
    {
        $query = WorkProcess::find()->where(['workcount_id' => $workcountIds]);

        $report = Report::findOne($report_id);
        $pks = $report->getReportQueuePks();
        $query->andFilterWhere(['report_id' => $pks]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * Create data provider instance from workcountIds
     *
     * @param array $workcountIds
     * @param integer $report_id
     *
     * @return ActiveDataProvider
     */
    public function searchByReport($report_id)
    {
        $query = WorkProcess::find();

        $report = Report::findOne($report_id);
        $pks = $report->getReportQueuePks();
        $query->andFilterWhere(['report_id' => $pks]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }
}
