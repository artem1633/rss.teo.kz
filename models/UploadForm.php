<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 5],
        ];
    }

    public function upload($report_id)
    {
        if ($this->validate())
        {
            foreach ($this->imageFiles as $file)
            {
                $name = 'uploads/' . $file->baseName . $report_id.'_'.time().'.' . $file->extension;
                $file->saveAs($name);


                $photo = new Photos();
                $photo->report_id = $report_id;
                $photo->photo = $name;
                $photo->save();
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}