<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_process".
 *
 * @property integer $id
 * @property integer $workcount_id
 * @property integer $report_id
 * @property integer $count_daily
 * @property string $date
 *
 * @property Report $report
 * @property Workcount $workcount
 */
class WorkProcess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_process';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workcount_id', 'count_daily'], 'required'],
            [['workcount_id', 'count_daily'], 'double'],
            [['date'], 'safe'],
            [['workcount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workcount::className(), 'targetAttribute' => ['workcount_id' => 'id']],
            [['report_id'], 'exist', 'skipOnError' => true, 'targetClass' => Report::className(), 'targetAttribute' => ['report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workcount_id' => 'Workcount ID',
            'count_daily' => 'Сделано сегодня',
            'date' => 'Дата и время создания',
        ];
    }

    /**
     * Получает сегодняшний результат выполнения работ, если результата нет,
     * то создается новая запись для результата работ
     * @return WorkProcess|array|null|\yii\db\ActiveRecord
     */
    public function findByCurrentDate()
    {
        $model = self::find()->where('date = CURDATE()')->one();

        if($model == null)
            $model = new self();

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkcount()
    {
        return $this->hasOne(Workcount::className(), ['id' => 'workcount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $workcount = $this->workcount;

        $workcount->count_fact = $workcount->count_fact - $this->count_daily;
        $workcount->work_id = strval($workcount->work_id);
        $workcount->save();

    }
}
