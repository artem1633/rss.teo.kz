<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 * @property string $login
 */
class ReportSearch extends Report
{

    public $login;
    public $position;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'post_send', 'prev_report'], 'integer'],
            [['done', 'project_id', 'object_id','creator_id', 'position', 'future_plan', 'problems', 'begin_work', 'creation_date', 'user_list', 'login'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'position' => [
                        'asc' => ['users.name' => SORT_ASC],
                        'desc' => ['users.name' => SORT_DESC],
                        'label' => 'ФИО',
                        'default' => SORT_ASC
                    ],
                    'id',
                    'creator_id',
                    'creation_date',
                    'begin_work',
                    'project_id',
                    'user_list',
                    'done',
                    'future_plan',
                    'problems',
                    'object_id' => ['objects.title' => SORT_ASC],
                ],
                'defaultOrder' => [
                    'creation_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->user_list != null) // Костыль для поиска по имени
        {
            /** @var \app\models\Users $user */
            $user = Users::find()->filterWhere(['like', 'name', $this->user_list])->one();

            if ($user != null) {
                $this->login = $user->login;
            } else {
                $this->login = null;
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'report.id' => $this->id,
            'post_send' => $this->post_send,
            'prev_report' => $this->prev_report,
            'users.role' => $this->position,
        ]);

        $query->joinWith('project');
        $query->joinWith('object');
        $query->joinWith('creator');

        $query->andFilterWhere(['like', 'done', $this->done])
            ->andFilterWhere(['like', 'future_plan', $this->future_plan])
            ->andFilterWhere(['like', 'project.title', $this->project_id])
            ->andFilterWhere(['like', 'object.title', $this->object_id])
            ->andFilterWhere(['like', 'users.name', $this->creator_id])
            ->andFilterWhere(['like', 'user_list', $this->login])
            ->andFilterWhere(['like', 'problems', $this->problems]);


        if (!is_null($this->creation_date) && strpos($this->creation_date, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->creation_date);
            $query->andFilterWhere(['between', 'report.creation_date', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }
        if (!is_null($this->begin_work) && strpos($this->begin_work, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->begin_work);
            $query->andFilterWhere(['between', 'report.begin_work', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }

        $this->addRulesConditions($query);

        return $dataProvider;
    }

    public function searchCustom($params)
    {
        $query = Report::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'position' => [
                        'asc' => ['users.name' => SORT_ASC],
                        'desc' => ['users.name' => SORT_DESC],
                        'label' => 'ФИО',
                        'default' => SORT_ASC
                    ],
                    'id',
                    'creator_id',
                    'creation_date',
                    'begin_work',
                    'project_id',
                    'user_list',
                    'done',
                    'future_plan',
                    'problems',
                    'object_id' => ['objects.title' => SORT_ASC],
                ],
                'defaultOrder' => [
                    'creation_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->user_list != null) // Костыль для поиска по имени
        {
            /** @var \app\models\Users $user */
            $user = Users::find()->filterWhere(['like', 'name', $this->user_list])->one();

            if ($user != null) {
                $this->login = $user->login;
            } else {
                $this->login = null;
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'report.id' => $this->id,
            'post_send' => $this->post_send,
            'prev_report' => $this->prev_report,
            'users.role' => $this->position,
        ]);

        $query->joinWith('project');
        $query->joinWith('object');
        $query->joinWith('creator');

        $query->andFilterWhere(['like', 'done', $this->done])
            ->andFilterWhere(['like', 'future_plan', $this->future_plan])
            ->andFilterWhere(['like', 'project.title', $this->project_id])
            ->andFilterWhere(['like', 'object.title', $this->object_id])
            ->andFilterWhere(['like', 'users.name', $this->creator_id])
            ->andFilterWhere(['like', 'user_list', $this->login])
            ->andFilterWhere(['like', 'problems', $this->problems]);


        if (!is_null($this->creation_date) && strpos($this->creation_date, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->creation_date);
            $query->andFilterWhere(['between', 'report.creation_date', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }
        if (!is_null($this->begin_work) && strpos($this->begin_work, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->begin_work);
            $query->andFilterWhere(['between', 'report.begin_work', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }

        $this->addRulesConditions($query);

        return $dataProvider;
    }

    public function searchExcellWeekly($params)
    {
        $query = (new Query())
            ->select([
               'user_name' => 'users.name',
                'position_name' => 'position.title',
                'report_done' => 'report.done',

            ])
            ->from('report')
            ->innerJoin('work_process','work_process.report_id=report.id')
            ->leftJoin('project','project.id=report.project_id')
            ->leftJoin('object' , 'report.object_id=object.id')

            ->leftJoin('workcount','work_process.workcount_id=workcount.id')
            ->leftJoin('work', 'workcount.work_id=work.id')
            ->leftJoin('section', 'workcount.section=section.id')

            ->innerJoin('users', 'report.creator_id=users.id')
            ->innerJoin('position', 'users.role=position.id')
            ->leftJoin('cars', 'cars.id=report.avto')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => 10000,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->user_list != null) // Костыль для поиска по имени
        {
            /** @var \app\models\Users $user */
            $user = Users::find()->filterWhere(['like', 'name', $this->user_list])->one();

            if ($user != null) {
                $this->login = $user->login;
            } else {
                $this->login = null;
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'report.id' => $this->id,
            'report.post_send' => $this->post_send,
            'report.prev_report' => $this->prev_report,
            'users.role' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'report.done', $this->done])
            ->andFilterWhere(['like', 'report.future_plan', $this->future_plan])
            ->andFilterWhere(['like', 'project.title', $this->project_id])
            ->andFilterWhere(['like', 'users.name', $this->creator_id])
            ->andFilterWhere(['like', 'report.user_list', $this->login])
            ->andFilterWhere(['like', 'report.problems', $this->problems]);


        if (!is_null($this->creation_date) && strpos($this->creation_date, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->creation_date);
            $query->andFilterWhere(['between', 'report.creation_date', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }
        if (!is_null($this->begin_work) && strpos($this->begin_work, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->begin_work);
            $query->andFilterWhere(['between', 'report.begin_work', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }

        $this->addRulesConditions($query);

        return $dataProvider;
    }

    public function searchExel($params)
    {
        $query = (new Query())
            ->select([
                'user_login' => 'users.login',
                'user_role' => 'users.role',
                'report_begin_work' => 'report.begin_work',
                'user_name' => 'users.name',
                'project_name' => 'project.title',
                'object_name' => 'object.title',
                'section_name' => 'section.title',
                'work_name' => 'work.title',
                'work_count' => 'work_process.count_daily',
                'work_units' => 'work.unit',
                'report_driving' => 'report.driving',
                'report_renta_avto' => 'report.renta_avto',
                'report_done' => 'report.done',
                'report_user_list' => 'report.user_list',
                'auto' => 'report.avto',
                'future_plan' => 'report.future_plan',
                'problems' => 'report.problems',

            ])
            ->from('report')
            ->innerJoin('work_process','work_process.report_id=report.id')
            ->leftJoin('project','project.id=report.project_id')
            ->leftJoin('object' , 'report.object_id=object.id')

            ->leftJoin('workcount','work_process.workcount_id=workcount.id')
            ->leftJoin('work', 'workcount.work_id=work.id')
            ->leftJoin('section', 'workcount.section=section.id')

            ->innerJoin('users', 'report.creator_id=users.id')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => 30000,
                'pageSize' => 10000,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->user_list != null) // Костыль для поиска по имени
        {
            /** @var \app\models\Users $user */
            $user = Users::find()->filterWhere(['like', 'name', $this->user_list])->one();

            if ($user != null) {
                $this->login = $user->login;
            } else {
                $this->login = null;
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'report.id' => $this->id,
            'report.post_send' => $this->post_send,
            'report.prev_report' => $this->prev_report,
            'users.role' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'report.done', $this->done])
            ->andFilterWhere(['like', 'report.future_plan', $this->future_plan])
            ->andFilterWhere(['like', 'project.title', $this->project_id])
            ->andFilterWhere(['like', 'users.name', $this->creator_id])
            ->andFilterWhere(['like', 'report.user_list', $this->login])
            ->andFilterWhere(['like', 'report.problems', $this->problems]);


        if (!is_null($this->creation_date) && strpos($this->creation_date, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->creation_date);
            $query->andFilterWhere(['between', 'report.creation_date', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }
        if (!is_null($this->begin_work) && strpos($this->begin_work, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->begin_work);
            $query->andFilterWhere(['between', 'report.begin_work', trim($start_date) . ' 00:00:00', trim($end_date) . ' 23:59:59']);
        }

        $this->addRulesConditions($query);

        return $dataProvider;
    }
    /**
     * Добавляет в условия поиска роли
     * @param \yii\db\Query $query
     */
    private function addRulesConditions(&$query)
    {
        $user = Yii::$app->user->identity;
        if ($user->rule == User::PERMISSIONS_EMPLOYEE) {
            $query->andFilterWhere(['creator_id' => $user->id]);
        }
        if ($user->rule == User::PERMISSIONS_PROJECTS_MANAGER) {
            $query->andFilterWhere(['or',
                ['creator_id' => $user->id],
                ['project.pm' => $user->id]
            ]);
        }
    }

    /**
     * Отадает рекорд модели из базы по id
     * @param $id
     * @return Report
     * @throws NotFoundHttpException
     */
    public static function findModel($id){
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не существует.');
        }
    }
}
