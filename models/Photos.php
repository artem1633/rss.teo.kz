<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property string $photo
 * @property string $report_id
 * @property string $original_name
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo', 'original_name'], 'string', 'max' => 255],
            [['report_id'],'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Photo',
            'report_id' => 'Report ID',
        ];
    }



    //=====================================================================================
    //--------------- СЕРВИСНЫЙ СЛОЙ --------------


    /**
     * Создает временную запись в базе с идентификатором отчета 0
     * Фактически запись со ссылкой на файл фотографии, но без привязки к какому-либо отчету
     * @param $path string Полный путь по которому хранится файл, включая имя файла и расширение
     * @param $original_name string оригинальное имя файла с расширением
     * @return bool
     */
    public static function createTempPhoto($path, $original_name){
        $record = new Photos();
        $record->photo = $path;
        $record->report_id = 0;
        $record->original_name = $original_name;
        return $record->save();
    }


    /**
     * Находит в базе временную запись по полному пути к файлу. Для этой записи выставляет report_id
     * @param $report_id
     * @param $full_path
     */
    public static function updateReportIdByPath($report_id, $full_path){
        $record = self::findModelByPath($full_path);
        /** @var $record Photos */
        if ($record) {
            //присваиваем новый идентификатор отчета
            $record->report_id = $report_id;

            $record->save();
        }
    }

    /**
     * Находит в базе модель по полному пути к файлу
     * @param $full_path string
     * @return array|null|\yii\db\ActiveRecord
     */
    private static function findModelByPath($full_path){
        return Photos::find()->where(['photo' => $full_path])->one();
    }


}
