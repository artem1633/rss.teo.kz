<?php

/**
 * Debug function
 * d($var);
 */
function d($var,$caller=null)
{
    if(!isset($caller)){
        $caller = array_shift(debug_backtrace(1));
    }
    echo '<code>File: '.$caller['file'].' / Line: '.$caller['line'].'</code>';
    echo '<pre>';
    yii\helpers\VarDumper::dump($var, 10, true);
    echo '</pre>';
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var)
{
    $tmpVar = array_keys(debug_backtrace(1));
    $caller = array_shift($tmpVar);
    d($var,$caller);
    die();
}


/**
 * Удобоваримый вывод при отладке
 * @param $var
 *
 * krbrs
 */
function pre($var){
    echo "<pre>" . print_r($var, true) . "</pre>";
}
