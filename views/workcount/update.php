<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkCount */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="work-count-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
