<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WorkCount */
/* @var $form yii\widgets\ActiveForm */
/* @var $project */

$project = Yii::$app->request->get('project_id');
$object = Yii::$app->request->get('object_id');
$model->project_id = $project;
$model->object_id = $object;
$section = Yii::$app->request->get('section');

$sectons = \app\models\Section::find()->where(['object_id'=>$object])->all();

if (!empty($sectons))
{
    $sectons = \yii\helpers\ArrayHelper::map($sectons,'id','title');
}
\yii\helpers\Url::remember();
?>

<div class="work-count-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => true,
                'validationUrl' => ['workcount/validate'],
            ]); ?>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'project_id')->hiddenInput(['value'=> $model->project_id])->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'object_id')->hiddenInput(['value'=> $model->object_id])->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'section')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data'=>$sectons,
                        'options' => ['value'=>$section,'placeholder' => 'Выберите раздел...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'work_id')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Work::find()->all(),'id','title'),
                        'options' => ['placeholder' => 'Выберите работу...', 'required' => 'true'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'count')->textInput() ?>
                </div>
            </div>

            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Назад в проект',['/project/update?id='.$model->project_id], ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php
$searchModel = new \app\models\WorkCountSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andWhere(['project_id' => $model->project_id]);
$dataProvider->query->andWhere(['object_id' => $model->object_id]);
?>
<?php \yii\widgets\Pjax::begin([
    'enablePushState'=>FALSE
]); ?>
<div class="box box-default">
    <div class="box-body" style="overflow-x: auto;">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'id'=>'grid-1',
            'columns' => [
                ['label' => 'Объект',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $object = \app\models\Object::findOne($data->object_id);
                        if (!empty($object))
                        {
                            return $object->title;
                        }
                        return 'Объект удалён';
                    }],
                ['label' => 'Раздел',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $object = \app\models\Section::findOne($data->section);
                        if (!empty($object))
                        {
                            return $object->title;
                        }
                        return 'Раздел удалён';
                    }],
                ['label' => 'Работа',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $model = \app\models\Work::findOne($data->work_id);
                        if (!$model) {
                            return "Работа удалена";
                        }
                        return $model->title;
                    }],
                ['label' => 'Ед. Изм.',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $model = \app\models\Work::findOne($data->work_id);
                        if (!$model) {
                            return "--";
                        }
                        return $model->unit;
                    }],
                ['label' => 'Количество',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->count;
                    }],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} '],
            ],
        ]); ?>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>
</div>