<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkCount */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Work Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-count-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
