<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkCount */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-count-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'project_id',
            'object_id',
            'section',
            'work_id',
            'units',
            'count',
        ],
    ]) ?>

</div>
