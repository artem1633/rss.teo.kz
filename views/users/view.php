<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            ['attribute' => 'rule',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Администратор',],
                        ['id' => 2,
                            'title' => 'Сотрудник',],
                        ['id' => 3,
                            'title' => 'Директор проектов',],
                        ['id' => 4,
                            'title' => 'Руководитель проекта',],
                    ], 'id', 'title')[$data->rule];
                }],
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => function ($data) {
                    $user = \app\models\Position::findOne($data->role);
                    return !empty($user) ? Html::a($user->title, ['position/view', 'id' => $data->role]) : 'Должность удалена';
                }
            ],
            'login',
            'password',
            'is_deleted',
            'created_at',
            'update_at',
        ],
    ]) ?>

</div>
