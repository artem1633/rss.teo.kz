<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="users-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'rule')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->getRule(),
                        'options' => ['placeholder' => 'Выберите права...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'role')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Position::find()->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Выберите Должность...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>				
			</div>
        </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'recipients')->label()->widget(\kartik\select2\Select2::classname(),
                        [
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Position::find()->all(), 'id',
                                'title'),
                            'options' => [
                                'placeholder' => 'Выберите список должностей которые могут отправлять вам почту...',
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                </div>
            </div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
