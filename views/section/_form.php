<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Section */
/* @var $form yii\widgets\ActiveForm */

$reference = new \app\models\Work();

?>

<div class="section-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true,'required']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'object_id')->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Object::find()->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Выберите объект...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
            </div>
            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
