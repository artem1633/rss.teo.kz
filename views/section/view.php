<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Section */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            ['attribute' => 'object_id',
                'format'=>'raw',
                'value' => function ($data) {
                    if ($data->object_id != null)
                    {
                        $object = \app\models\Object::findOne($data->object_id);
                        return Html::a($object->title, ['object/view', 'id' => $object->id]);
                    }
                    return 'Объект не выбран';
                }],
        ],
    ]) ?>

</div>
