<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 vcenter row">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <div class="form-group">
                        <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
                            'language' => 'ru',
                            'dateFormat' => 'php:d.m.Y',
                            'options' => ['class' => 'form-control'],
                        ]); ?>
                    </div>
                </div>
            </div>
            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
