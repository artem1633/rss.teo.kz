<?php
/**
 * @var \app\models\Report $report
 * @var integer $report_id
 * @var integer $has_works
 * @var string $objectTitle
 * @var string $projectTitle
 * @var string $usersList
 * @var string $creatorName
 * @var string $creatorPosition
 * @var string $creatorEmail
 * @var string $creatorPhone
 * @var $processesDataProvider
 * @var \app\models\Workcount workcounts
 * @var \app\models\Object $objects
 * @var \app\models\Section $sections
 * @var \app\models\Workcount $workCounts
 */
?>

<span style="color: teal;">Отчет за:</span> <?=\DateTime::createFromFormat('Y-m-d H:i:s', $report->begin_work)->format('d.m.Yг.')?> <br><br>

<span style="color: teal;">Проект:</span> <?=$projectTitle?> <br>
<span style="color: teal;">Объект:</span> <?=$objectTitle?> <br>
<span style="color: teal;">Состав бригады:</span> <?=$usersList?> <br>
<span style="color: teal;">Выполнено:</span> <br> <?=stripslashes(nl2br($report->done))?> <br> <br>
<span style="color: teal;">Планы на следущий день: </span> <br> <?=stripslashes(nl2br($report->future_plan))?> <br> <br>
<span style="color: teal;">Проблемы: </span> <br> <?=$report->problems == null ? "Отсутствуют" : stripslashes(nl2br($report->problems))?> <br> <br>
<span style="color: teal;">Автомобиль: </span> <br>
<?=stripslashes(nl2br($avto))?> <br>
Водительские: <?=stripslashes(nl2br($driving))?> км<br>
Аренда машины: <?=stripslashes(nl2br($renta_avto))?> км<br>


<br> <br>
<?php if($has_works == 1): ?>

    <span style="color: teal;">Работы</span>
    <?=$this->render('@app/views/report/_work_table', [
        'processesDataProvider' => $processesDataProvider,
        'workcounts' => $workcounts,
        'objects' => $objects,
        'works' => $works,
        'sections' => $sections,
        'report_id' => $report_id,
    ])?>

<?php endif; ?>
<br> <br> <br>
-- <br>
<b><span style="color: #b42d2d"><?=$creatorName?></span></b> <br>
<i><?=$creatorPosition?></i> <br>
<i>ООО «РСС Инжиниринг»</i> <br>
<b><i style="color:#0070c0;"><?=$creatorEmail?></i></b> <br>
<b><?=$creatorPhone?></b>