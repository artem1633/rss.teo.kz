<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */

$works = new \app\models\ObjectSection();

$alreadySelected = \app\models\ProjectObject::findAll(['project_id' => $model->id]);
$selected = [];
$users = \app\models\Users::find()->all();
foreach ($alreadySelected as $items)
{
    $selected[] = $items->object->id;
}

$data = \app\models\Object::find()->all();
$result = [];
foreach ($data as $item)
{
    $result[$item->id] = $item->title;
}

\yii\helpers\Url::remember();
?>

<div class="project-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'pm')->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map($users, 'id', 'name'),
                        'options' => ['placeholder' => 'Выберите руководителя...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <!-- <div class="col-md-3 vcenter">
                    <?php /* $form->field($refe, 'object_id')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Object::find()->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Выберите Объект...', 'multiple' => true,
                            'value' => $selected,],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); */?>
                </div> -->
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'command')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map($users, 'login', 'name'),
                        'options' => ['placeholder' => 'Выберите состав группы проекта...', 'multiple' => true,],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>
            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['name' => 'button1','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::submitButton($model->isNewRecord ? 'Выбрать файл ВР ' : 'Изменить', ['name' => 'button2','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?=$model->id ?Html::a('Добавить отчёт', ['/report/create?project_id='.$model->id],['class'=>'btn btn-success']):''?>
                
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>


    <?php
    $searchModel = new \app\models\ProjectobjectSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->query->andWhere(['project_id' => $model->id]);
    ?>
    <div class="box box-default">
        <div class="box-body" style="overflow-x: auto;">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['label' => 'Объект',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return \app\models\Object::findOne($data->object_id)->title;
                        }],
                    ['label' => 'Добавить работу',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a('Добавить работу', ['/workcount/create?object_id=' . $data->object_id . '&project_id=' . $data->project_id],['target'=>'_blank']);
                        }],
                ],
            ]); ?>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <!— USERS LIST —>
        <div class="box box-danger collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Работы</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!— /.box-header —>
            <div class="box-body no-padding" style="display: none;">
                <?php
                $searchModel = new \app\models\WorkCountSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->andWhere(['project_id' => $model->id]);
                ?>
                <?php \yii\widgets\Pjax::begin([
                    'enablePushState'=>FALSE
                ]); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'id'=>'grid-1',
                            'rowOptions' => function($data){
                                if($data->count_fact >= $data->count){
                                    return ['class' => 'success'];
                                }
                            },
                            'columns' => [
                                ['label' => 'Объект',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        $object = \app\models\Object::findOne($data->object_id);
                                        if (!empty($object))
                                        {
                                            return $object->title;
                                        }
                                        return 'Объект удалён';
                                    }],
                                ['label' => 'Раздел',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        $object = \app\models\Section::findOne($data->section);
                                        if (!empty($object))
                                        {
                                            return $object->title;
                                        }
                                        return 'Раздел удалён';
                                    }],
                                ['label' => 'Работа',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return \app\models\Work::findOne($data->work_id)->title;
                                    }],
                                ['label' => 'Ед. Изм.',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return \app\models\Work::findOne($data->work_id)->unit;
                                    }],
                                ['label' => 'Количество',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->count;
                                    }],
                                [
                                    'label' => 'Выполнено',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->count_fact;
                                    },
                                ],
                                [
                                    'label' =>'Action',
                                    'value' => function ($model)
                                    {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/workcount/delete', 'id' => $model->id], ['data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),'method' => 'post',],]);
                                    },
                                    'format'=>'raw',
                                ],
                            ],
                        ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>

            </div>
            <div class="box-footer text-center" style="display: none;">

            </div>
        </div>
        <!--/.box —>
      </div>
</div>