<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Uploads */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uploads-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'File')->fileInput() ?>

    <div style="display:none;">
    	<?= $form->field($model, 'proyekt_id')->textInput(['maxlength' => true]) ?>
	</div>    

    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Импорт' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
