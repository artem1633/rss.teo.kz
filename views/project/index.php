<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">
    <div class="box box-default">
        <div class="box-body">
            <!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-body" style="overflow-x: auto;">
        <?= GridView::widget([
            'responsiveWrap' => false,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'content' => function ($data) {
                        return '<span class="label label-warning" style="font-size:13px;">' . Html::a($data->id . '   ', ['update', 'id' => $data->id], ['style' => 'color:#ffffff;']) . '</span>';

                    },
                ],

                // 'id',
                'title',
                [
                    'attribute' => 'pm',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $user = \app\models\Users::findOne($data->pm);
                        return !empty($user) ? Html::a($user->name , ['users/view', 'id' => $data->pm]): 'Пользователь удалён';
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} {view}'],
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
</div>
