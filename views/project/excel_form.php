<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */

$works = new \app\models\ObjectSection();

$alreadySelected = \app\models\ProjectObject::findAll(['project_id' => $model->id]);
$selected = [];
$users = \app\models\Users::find()->all();
foreach ($alreadySelected as $items)
{
    $selected[] = $items->object->id;
}

$data = \app\models\Object::find()->all();
$result = [];
foreach ($data as $item)
{
    $result[$item->id] = $item->title;
}

\yii\helpers\Url::remember();
?>

<div class="project-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                </div>
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'pm')->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map($users, 'id', 'name'),
                        'disabled' => 'disabled',
                        'options' => ['placeholder' => 'Выберите руководителя...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <!-- <div class="col-md-3 vcenter">
                    <?php  /*$form->field($refe, 'object_id')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Object::find()->all(), 'id', 'title'),
                        'disabled' => 'disabled',
                        'options' => ['placeholder' => 'Выберите Объект...', 'multiple' => true,
                            'value' => $selected,],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);*/ ?>
                </div> -->
                <div class="col-md-3 vcenter">
                    <?= $form->field($model, 'command')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map($users, 'login', 'name'),
                        'disabled' => 'disabled',   
                        'options' => ['placeholder' => 'Выберите состав группы проекта...', 'multiple' => true,],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>
            <div style="display:none">
            </div>
            <div class="form-group">
                <?php //$model->id ?Html::a('Проверить файл', ['/project/getexcel', 'id' => $model->id, 'testing' => '1'],['class'=>'btn btn-success']):''?>
                <?=$model->id ?Html::a($excel_data != null ? 'Добавить ВР в проект' : 'Назад', ['/project/getexcel', 'id' =>$model->id, 'testing' => '2'],['class'=>'btn btn-success']):''?>
                <?=$retry == 1 ?Html::a('Загрузить новый файл ВР', ['/project/upload', 'id' =>$model->id],['class'=>'btn btn-success']):''?>              
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>


    <div class="box box-default">
        <div class="box-body" style="overflow-x: auto;">
            <?php if($excel_data != null) { ?>
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th style="font-size: 15px;" >Проект</th>
                        <th style="font-size: 15px;" >Объект</th>  
                        <th style="font-size: 15px;" >Раздел ПД</th>
                        <th style="font-size: 15px;" >Наименование работы</th> 
                        <th style="font-size: 15px;" >Ед. измерения</th>
                        <th style="font-size: 15px;" >Кол-во</th>                              
                    </tr> 
                <?php
                  foreach ($excel_data as $value) {
                      echo '<tr>'; 
                      foreach ($value as $key) {
                         echo "<td width = 20px >".$key."</td>";
                      }
                      echo "</tr>";
                    }  
                  ?>
                  </table>
            <?php } 
            else { echo '<h2 style=" color:red;"><center>Выбранном excel файле нет новых записей</center</h2>'; }
            ?>
        </div>
    </div>
</div>

