<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $refe app\models\ProjectObject */

$refe = new \app\models\ProjectObject();

$this->title = 'Изменить ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="project-update">


    <?= $this->render('update_form', [
        'model' => $model,
        'refe'=>$refe,
    ]) ?>

</div>
