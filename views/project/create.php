<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $refe app\models\ProjectObject */

$refe = new \app\models\ProjectObject();
$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">


    <?= $this->render('_form', [
        'refe'=>    $refe,
        'model' => $model,
    ]) ?>

</div>
