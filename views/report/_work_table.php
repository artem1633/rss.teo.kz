<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;

if(Yii::$app->requestedAction->id == 'update')
{
    $report_id = Yii::$app->request->get('id');
}

?>

<?= GridView::widget([
    'dataProvider' => $processesDataProvider,
    'responsiveWrap' => false,
    'tableOptions' => ['border' => 1],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'label' => 'Объект',
            'content' => function($data) use ($workcounts, $objects){
                foreach ($workcounts as $workcount)
                {
                    if($data->workcount_id == $workcount->id)
                    {
                        foreach ($objects as $object)
                        {
                            if($workcount->object_id == $object->id)
                                return $object->title;
                        }
                    }
                }
            }
        ],
        [
            'label' => 'Раздел',
            'content' => function($data) use ($workcounts, $sections){
                foreach ($workcounts as $workcount)
                {
                    if($data->workcount_id == $workcount->id)
                    {
                        foreach ($sections as $section)
                        {
                            if($workcount->section == $section->id)
                                return $section->title;
                        }
                    }
                }
            }
        ],
        [
            'label' => 'Работа',
            'content' => function($data) use ($workcounts, $works){
                foreach ($workcounts as $workcount)
                {
                    if($data->workcount_id == $workcount->id)
                    {
                        foreach ($works as $work)
                        {
                            if($workcount->work_id == $work->id)
                                return $work->title;
                        }
                    }
                }
            }
        ],
        [
            'label' => 'Всего по проекту',
            'content' => function($data) use ($workcounts){
                foreach ($workcounts as $workcount)
                {
                    if($data->workcount_id == $workcount->id)
                    {
                        return $workcount->count;
                    }
                }
            }
        ],
        [
            'label' => 'Ед.изм.',
            'content' => function($data) {
                return $data->workcount->units;
            }
        ],
        [
            'label' => 'Сделано ранее',
            'content' => function($data) use ($workcounts){
                foreach ($workcounts as $workcount)
                {
                    if($data->workcount_id == $workcount->id)
                    {
                        return $workcount->count_fact;
                    }
                }
            }
        ],
        'count_daily',
        [
            'attribute' => 'date',
            'content' => function ($data) {
                if ($data->date) {
                    return \DateTime::createFromFormat('Y-m-d H:i:s', $data->date)->format('d.m.Y H:i:s');
                } else {
                    return false;
                }

            },
        ],

         ['class' => 'yii\grid\ActionColumn',
             'template' => '{delete}',
             'buttons' => [
                 'delete' => function($url, $model) use ($report_id){
                     $url = Url::to(['report/delete-process', 'id' => $model->id, 'report_id' => $report_id]);
                     return Html::a('<i class="fa fa-trash text-danger" style="font-size: 18px;"></i>', $url, ['title' => 'Удалить']);
                 },
             ],
             'visible' => $report_id != null ? true : false,
         ],
    ],
]); ?>