<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $works app\models\Work[] */
/* @var $objects app\models\Object[] */
/* @var $sections app\models\Section[] */
/* @var $newWork app\models\Workcount */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Отчеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-create">


    <?= $this->render('_form', [
        'model' => $model,
        'works' => $works,
        'sections' => $sections,
        'newWork' => $newWork,
        'workcounts' => $workcounts,
        'objects' => $objects,
        'processesDataProvider' => $processesDataProvider,
    ]) ?>

</div>
