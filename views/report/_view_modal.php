<?php
/**
 * @var \app\models\Report $model
 */
?>
<div class="row">
    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('project_id') ?></h4>
        <p><?= stripslashes(nl2br($model->project->title)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('object_id') ?></h4>
        <p><?= stripslashes(nl2br($model->object->title)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('begin_work') ?></h4>
        <p><?= stripslashes(nl2br(\DateTime::createFromFormat('Y-m-d H:i:s', $model->begin_work)->format('d.m.Y'))) ?></p>
    </div>

</div>
<div class="row">
    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('done') ?></h4>
        <p><?= stripslashes(nl2br($model->done)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('future_plan') ?></h4>
        <p><?= stripslashes(nl2br($model->future_plan)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('problems') ?></h4>
        <p><?= stripslashes(nl2br($model->problems)) ?></p>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('driving') ?></h4>
        <p><?= stripslashes(nl2br($model->driving)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('renta_avto') ?></h4>
        <p><?= stripslashes(nl2br($model->renta_avto)) ?></p>
    </div>

    <div class="col-md-4">
        <h4><?= $model->getAttributeLabel('avto') ?></h4>
        <p><?= stripslashes(nl2br(isset($model->avtomobile->name)?$model->avtomobile->name:"")) ?></p>
    </div>

</div>
<?php if ($model->works): ?>
<div class="row">
    <?php foreach ($model->works as $key => $work): ?>
        <?php if (!$key): ?>
            <div class="col-md-3">
                <h4><?= $work->workcount->getAttributeLabel('work_id') ?></h4>

            </div>

            <div class="col-md-3">
                <h4><?= $work->workcount->getAttributeLabel('count') ?></h4>

            </div>

            <div class="col-md-3">
                <h4><?= $work->workcount->getAttributeLabel('count_fact') ?></h4>

            </div>
            <div class="col-md-3">
                <h4><?= $work->getAttributeLabel('count_daily') ?></h4>

            </div>
        <?php endif; ?>
        <div class="col-md-3">

            <p><?= stripslashes(nl2br($work->workcount->work->title)) ?></p>
        </div>

        <div class="col-md-3">

            <p><?= stripslashes(nl2br($work->workcount->count)) ?></p>
        </div>

        <div class="col-md-3">

            <p><?= stripslashes(nl2br($work->workcount->count_fact)) ?></p>
        </div>
        <div class="col-md-3">

            <p><?= stripslashes(nl2br($work->count_daily)) ?></p>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>