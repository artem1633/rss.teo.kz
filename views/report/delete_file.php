<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Report;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
/* @var $works app\models\Work[] */
/* @var $objects app\models\Object[] */
/* @var $sections app\models\Section[] */
/* @var $photos app\models\Photos[] */
/* @var $newWork app\models\Workcount */
$this->title = 'Файлы';

$css = "
.bootstrap-tagsinput{
    width: 100%;
}

.bootstrap-tagsinput .tag{
    font-size: 12px !important;
}

";

$js = "

var sw = getParameterByName('sw');

if(sw == 1)
{
    $('#report-has_works').val(1);
}

$('#btn-works').click(function(e){
    e.preventDefault();
    $('#report-has_works').val(1);
    $('.works').show();
    $(this).hide();
});

$('#works-report_ww').click(function(e){
    e.preventDefault();
    $('#report-is_send_report').val(1);
    $('#works-form').submit();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, \"\\$&\");
    var regex = new RegExp(\"[?&]\" + name + \"(=([^&#]*)|&|#|$)\"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, \" \"));
}

setTimeout(function(){
console.log($('.bootstrap-tagsinput input').attr('style', 'width: 10em !important;'));
}, 1000);

";

$this->registerCss($css);
$this->registerJs($js, \yii\web\View::POS_READY);
?>
<div class="project-index">
    <div class="box box-default">
        <div class="box-body">
            <!-- <img src="https://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Назад', ['update', 'id' => $report_id], ['class' => 'btn btn-success']) ?>
            </p>

        </div>
    </div>
</div>

<div class="report-form">
    <div class="box box-default">
        <div class="box-body">
            <div class="photos">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <!-- <th><center>Отчёт ID</center></th> -->
                        <th><center>Файл</center></th>
                        <th><center>Операция</center></th>
                    </tr>
                    <?php \app\assets\MagnificPopupAsset::register($this) ?>
                    <?php foreach($photos as $photo): ?>
                        <tr>
                            <!-- <td><center>1</center></td> -->
                            <td><center><img style="width: 150px; height: 150px; object-fit: cover; margin-top: 10px; cursor: pointer;" src="https://<?= $_SERVER['SERVER_NAME'].'/'.$photo->photo ?>" href="http://<?= $_SERVER['SERVER_NAME'].'/'.$photo->photo ?>">
                            </center></td>
                            <td><center style="margin-top:70px;"><span class="label label-warning" style="font-size:13px; "><?=Html::a("Удалить файл".'   ', ['filedelete','id' =>$report_id, 'photo' => $photo->id], ['style'=>'color:#ffffff;'])?></span></center></td>
                            
                        </tr>
                    <?php endforeach; ?>  
                </table>              
            </div>
        </div>
    </div>
</div>

<?php 

$galleryScript = "
    $('.photos').magnificPopup({
        delegate: 'img',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1], // Will preload 0 - before current, and 1 after the current image   
            tPrev: 'Предыдущая', // title for left button
            tNext: 'Следующая', // title for right button
            tCounter: '<span class=\"mfp-counter\">%curr% из %total%</span>' // markup of counter
        },
        image: {
            tError: '<a href=\"%url%\">Изображение #%curr%</a> не может быть загружено.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });
";

    $this->registerJs($galleryScript, \yii\web\View::POS_READY);

?>
