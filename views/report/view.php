<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'done:ntext',
            [
                'attribute' => 'project_id',
                'format' => 'raw',
                'value' => function ($data) {
                    $user = \app\models\Project::findOne($data->project_id);
                    return !empty($user) ? Html::a($user->title, ['users/view', 'id' => $data->project_id]) : 'Проект удалён';
                }
            ],
            'future_plan:ntext',
            'problems:ntext',
            'creator_id',
            'creation_date',
            'post_send',
            'prev_report',
            'user_list',
            [
                'label' => 'Список объектов',
                'attribute' => 'project_id',
                'format' => 'raw',
                'value' => function ($data) {
                    $projects = \app\models\ProjectObject::findAll(['project_id' => $data->project_id]);
                    foreach ($projects as &$item)
                    {
                        $project = \app\models\Object::findOne($item->object_id);
                        $item = Html::a($project->title, ['object/view', 'id' => $project->id]);
                    }

                    return implode(',', $projects);
                }
            ],
            [
                'label' => 'Список разделов',
                'attribute' => 'project_id',
                'format' => 'raw',
                'value' => function ($data) {
                    $projects = \app\models\ProjectObject::findAll(['project_id' => $data->project_id]);
                    $sections = [];
                    foreach ($projects as &$item)
                    {
                        $project = \app\models\Object::findOne($item->object_id);
                        $section = \app\models\Section::findAll(['object_id' => $item->object_id]);
                        foreach ($section as $inside)
                        {
                            $sections[] = Html::a($inside->title, ['section/view', 'id' => $inside->id]);
                        }
                    }

                    return implode(',', $sections);
                }
            ],

            [
                'label' => 'Список работ',
                'attribute' => 'project_id',
                'format' => 'raw',
                'value' => function ($data) {
                    $projects = \app\models\ProjectObject::findAll(['project_id' => $data->project_id]);
                    $sections = [];
                    foreach ($projects as &$item)
                    {
                        $section = \app\models\Section::findAll(['object_id' => $item->object_id]);
                        foreach ($section as $inside)
                        {
                            $works = \app\models\WorkSection::findAll(['section_id' => $inside->id]);
                            foreach ($works as $work)
                            {
                                $work = \app\models\Work::findOne($work->work_id);
                                $sections[] = Html::a($work->title, ['work/view', 'id' => $work->id]);
                            }
                        }
                    }

                    return implode(',', $sections);
                }
            ],
            [
                'label' => 'Фото',
                'format' => 'raw',
                'value' => function ($data) {
                    $photos = \app\models\Photos::findAll(['report_id' => $data->id]);
                    $return='';
                    foreach ($photos as $photo)
                    {
                        $return.='<img src="'.\yii\helpers\Url::base().'/'.$photo->photo.'" width="600px" height="600px" alt="">';
                    }

                    return $return;
                }
            ],
        ],
    ]) ?>

</div>
