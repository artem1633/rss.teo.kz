<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use app\widgets\ExportMenuRss as ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $usersMap array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчёты';
$this->params['breadcrumbs'][] = $this->title;

$modalLoadScript = <<< JS
    function show_in_modal(addr){
        $('#modal-info').modal('show').find('#modal-content').load(addr);
        return false;
    };
JS;

$this->registerJs($modalLoadScript, \yii\web\View::POS_HEAD);

$thFilter = "
    <button id='filterButton'><i class=\"fa fa-filter\"></i></button>
  <div class='tooltip-my-div' data-toggled>
      <a class=\"toggling\" data-col-number=\"1\"><i data-cs-number=\"1\" class=\"fa fa-eye\"></i>ID Отчёта</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"2\"><i data-cs-number=\"2\" class=\"fa fa-eye\"></i>Дата работ</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"4\"><i data-cs-number=\"4\" class=\"fa fa-eye\"></i>Проект</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"5\"><i data-cs-number=\"5\" class=\"fa fa-eye\"></i>Объект</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"6\"><i data-cs-number=\"6\" class=\"fa fa-eye\"></i>ФИО</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"7\"><i data-cs-number=\"7\" class=\"fa fa-eye\"></i>Состав бригады</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"8\"><i data-cs-number=\"8\" class=\"fa fa-eye\"></i>Сделано за день</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"9\"><i data-cs-number=\"9\" class=\"fa fa-eye\"></i>План на следующий день</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"10\"><i data-cs-number=\"10\" class=\"fa fa-eye\"></i>Проблемы</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"11\"><i data-cs-number=\"11\" class=\"fa fa-eye\"></i>Должность</a>
                <br/>
                <a class=\"toggling\" data-col-number=\"12\"><i data-cs-number=\"12\" class=\"fa fa-eye\"></i>Дата создания</a>
    </div>
";


require_once ('_column.php');
?>

<div class="tooltip-old hidden"></div>

<div class="report-index">
    <div class="box box-default">
        <div class="box-body">
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                <?php
                $latest = \app\models\News::find()->all();

                if ($latest){

                $latest = $latest[count($latest) - 1];

                ?>

            <h3><b><?= $latest->title ?></b></h3>
            <h4><pre><?= strlen($latest->content) > 500 ? substr_replace($latest->content,
                    "....<a href=\"/news/view?id=" . $latest->id . "\"> читать далее</a>", 501)
                    : $latest->content . "<br/><br/><a href=\"/news/view?id=" . $latest->id."\">Больше информации</a>"
                ?>
                </pre>
            </h4>
            <?php
            }
            ?>
            </p>
            <p>
            </p>
        </div>
        <div class="box-body">
            <?= Html::a('Ежедневный отчет',
                ['daily', $_GET],
                [
                    'class' => 'btn btn-primary',
                    'method' => 'post',
                    'id' => 'daily-export',
                ]
            ) ?>
            <?php if (in_array(Yii::$app->user->identity->rule,
                [\app\models\User::PERMISSIONS_ADMIN, \app\models\User::PERMISSIONS_DIRECTOR_PROJECTS, \app\models\User::PERMISSIONS_PROJECTS_MANAGER])) {
                ?>
                <?= Html::a('Статистика по сданным/несданным отчетам',
                    ['stats', $_GET],
                    [
                        'class' => 'btn btn-primary',
                        'method' => 'post',
                        'id' => 'stat-export',
                    ]
                ) ?>
            <?php } ?>
        </div>
        <?php \yii\widgets\Pjax::begin((['id' => 'example']))?>
        <div class="box-body">
            <?= GridView::widget([
                'id' => 'example',
                'responsiveWrap' => false,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function ($model) {
                    return count($model->works) <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                'columns' => $columnsGrid,
            ]); ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'header' => 'Подробная информация',
    'id' => 'modal-info',
    'size' => 'modal-lg',
]) ?>
<div id="modal-content"></div>
<?php Modal::end() ?>
<?php

$modalLoadScript = <<< JS
                $('.toggling').click(function(e) {
                    e.preventDefault();
                    var bid = $(this).data('col-number');
                    
                    var open = 'fa fa-eye';
                    var closed = 'fa fa-eye-slash';
                    
                    
                    var column = $('[data-col-seq=' + bid + ']');
                    
                    $(this).find("i").toggleClass(open, closed);
                    $(this).find("i").toggleClass(closed, open);
                    column.toggle();
                    $('#example-filters').find('td').eq(bid).toggle();
                });
JS;

$this->registerJs($modalLoadScript, \yii\web\View::POS_READY);

$modalLoadScript = <<< JS
        $('#filterButton').click(function(){
                var pos = $(this).position();
                var fadelist = $('.tooltip-my-div');
                
                if(fadelist.data('toggled') == ''){
                    fadelist.css('top', (pos.top)+50 + 'px').css('left', (pos.left)+40 + 'px').fadeIn();
                    fadelist.data('toggled', 'true');
                }else{
                    fadelist.fadeOut();
                    fadelist.data('toggled', '');
                }
        });

        $('body').click(function(evt){
             var fadelist = $('.tooltip-my-div');
             var pos = $('#filterButton').position();
             
               if(evt.target.class == "tooltip-my-div"){
                  return;
               }
                  
              if(evt.target.id == "filterButton"){
                return;
              }
               if($(evt.target).closest('#filterButton').length){
                 return;             
              }
        
               if($(evt.target).closest('.tooltip-my-div').length){
                  return;
               }
                              
              if(fadelist.data('toggled') != ''){
                    fadelist.fadeOut();
                    fadelist.data('toggled', '');
              }
        
        });
JS;

$this->registerJs($modalLoadScript, \yii\web\View::POS_READY);

$clearDateRangePicker = <<< JS
    $('#reportsearch-begin_work').on('cancel.daterangepicker', function(ev, picker) {
      //do something, like clearing an input
      $('#reportsearch-begin_work').val('');
      $('.grid-view').yiiGridView('applyFilter');
    });
JS;
$this->registerJs($clearDateRangePicker);

$clearDateRangePicker = <<< JS
    $('#reportsearch-creation_date').on('cancel.daterangepicker', function(ev, picker) {
      //do something, like clearing an input
      $('#reportsearch-creation_date').val('');
      $('.grid-view').yiiGridView('applyFilter');
    });
JS;
$this->registerJs($clearDateRangePicker);

?>
<?php \yii\widgets\Pjax::end(); ?>

<style>
    .tooltip-my-div{
        cursor:pointer;
        color:#fff;
        padding:7px;
        border-radius:5px;
        position:absolute;
        min-width:50px;
        max-width:300px;
        display:none;
        background: #727171;
        border: 1px solid #ccc;
    }

    .tooltip-my-div a{
        color:white;
    }
    .ul div:before{
        content:'';
        height:3px;
        width:0;
        border:7px solid transparent;
        border-bottom-color:#1d1d1d;
        position:absolute;
        top:-12px;
        left:14px;
    }
</style>

<?php
$modalLoadScript = <<< JS
$(document).on('pjax:beforeSend', function() {
    var tooltipOld = $('.tooltip-my-div').clone();
    
    $('.tooltip-old').html(tooltipOld);
});

$(document).on('pjax:end', function() {
    var tooltipOld = $('.tooltip-old').children('div').contents();
    $('.tooltip-old').html('');
    
    $('.tooltip-my-div').html(tooltipOld);
    
    var open = 'fa fa-eye';
    var closed = 'fa fa-eye-slash';
    
   $( ".fa-eye-slash" ).each(function(i, obj) {
        bid = $(obj).data('cs-number');
        column = $('[data-col-seq=' + bid + ']');
        
        column.toggle();
        $('#example-filters').find('td').eq(bid).toggle(); 
    });
   
   var urlParams = new URLSearchParams(window.location.search);
   
   var baseHref = $('#stat-export').attr('href');
   $('#stat-export').attr('href', '/report/stats?'+urlParams.toString());
   $('#daily-export').attr('href', '/report/daily?'+urlParams.toString());
  

    $('.toggling').click(function(e) {
    e.preventDefault();
    var bid = $(this).data('col-number');
    
    var open = 'fa fa-eye';
    var closed = 'fa fa-eye-slash';
    
    
    var column = $('[data-col-seq=' + bid + ']');
    
    $(this).find("i").toggleClass(open, closed);
    $(this).find("i").toggleClass(closed, open);
    column.toggle();
    $('#example-filters').find('td').eq(bid).toggle();
});

});
JS;

$this->registerJs($modalLoadScript, \yii\web\View::POS_READY);
?>

