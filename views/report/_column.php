<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$columnsGrid = [
    //['class' => 'yii\grid\SerialColumn'],

    [
        'encodeLabel' => false,
        'label' => $thFilter,
        'attribute' => 'copy',
        'content' => function ($data) {
            return Html::a('<i class="fa fa-copy"></i>', ['report/create', 'original_id' => $data->id],[
                'class' => 'btn btn-sm btn-default','title' => 'Скопировать отчет'
            ]);

        },
        'width' => '10px',
    ],

    [
        'attribute' => 'id',
        'content' => function ($data) {
            return '<span class="label label-warning" style="font-size:13px;">' . Html::a($data->id . '   ', ['update', 'id' => $data->id], ['style' => 'color:#ffffff;']) . '</span>';

        },
        'width' => '10px',
    ],
    [
        'label' => 'Дата работ',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => ([
            'attribute' => 'begin_work',
            'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'Y-m-d',
                'opens' => 'right',

                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'Y-m-d',],
            ],
        ]),
        'attribute' => 'begin_work',
        'content' => function ($model) {
            if ($model->begin_work) {
                return \DateTime::createFromFormat('Y-m-d H:i:s', $model->begin_work)->format('d.m.Y');
            } else {
                return false;
            }

        },
        'width' => '130px',
    ],
    [
        'content' => function ($model) {
            return '<a class="modal-show" onClick="return show_in_modal(\''.Url::toRoute(['report/view-in-modal', 'id' => $model->id]).'\');" href="' . Url::toRoute(['report/view-in-modal', 'id' => $model->id]) . '"><i class="fa fa-eye"></i></a>';
        },
        'width' => '10px',
    ],
    [
        'attribute' => 'project_id',
        'format' => 'raw',
        'value' => function ($data) {
            $user = \app\models\Project::findOne($data->project_id);
            return !empty($user) ? Html::a($user->title, ['project/update', 'id' => $data->project_id]) : ' Проект удалён';
        },
        'width' => '130px',
    ],

    //объект по которому сделан отчет
    [
        'attribute' => 'object_id',
        'format'  => 'html',
        'value' => function ($data) {
            /** @var \app\models\Object  $object */
            $object = $data->object;

            if ( !$object) {
                return "Объект удален";
            }

            return Html::a($object->title, ['object/view' , 'id' => $object->id]);

        }
    ],


    ['label' => 'ФИО',
        'attribute' => 'creator_id',
        'content' => function ($model) use ($usersMap) {
            foreach ($usersMap as $id => $name) {
                if ($model->creator_id == $id)
                    return $name;
            };
        },
    ],

    [
        'label' => 'Состав бригады',
        'attribute' => 'user_list',
        'content' => function ($model) use ($usersMapByLogins) {
            $content = "";
            foreach ($model->user_list as $user) {
                $liText = isset($usersMapByLogins[$user]) ? $usersMapByLogins[$user] : '<span class="text-danger">Пользователь не найден</span>';
                $content .= "<li>" . $liText . "</li>";
            }
            $content = "<ul style='padding-left: 15px;'>" . $content . "</ul>";

            return $content;
        },
    ],
    // 'id',
    [
        'label' => 'Сделано за день',
        'attribute' => 'done',
        'content' => function ($model) {
            if ($model->done != null) {
                return iconv_substr($model->done, 0, 150, "UTF-8") . '...';
            }
        },
    ],
    [
        'attribute' => 'future_plan',
        'content' => function ($model) {
            if ($model->future_plan != null) {
                return iconv_substr($model->future_plan, 0, 150, "UTF-8") . '...';
            }
        },
    ],
    [
        'attribute' => 'problems',
        'content' => function ($model) {
            if ($model->problems) {
                return iconv_substr($model->problems, 0, 150, "UTF-8") . '...';
            }
        },
    ],

    [
        'label' => 'Должность',
        'filter' => \app\models\Position::positionsList(),
        'attribute' => 'position',
        'content' => function ($model) {
            return isset($model->creator->position->title) ? $model->creator->position->title : "";
        },
    ],


    [
        'attribute' => 'creation_date',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => ([
            'attribute' => 'creation_date',
            'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'Y-m-d',
                'opens' => 'left',

                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'Y-m-d',],
            ],
        ]),
        'content' => function ($model) {
            return \DateTime::createFromFormat('Y-m-d H:i:s', $model->creation_date)->format('d.m.Y H:i:s');
        },
    ],
    // 'creator_id',
    // 'creation_date',
    // 'post_send',
    // 'prev_report',
    ['class' => 'yii\grid\ActionColumn',
        'template' => '{delete}'],
    // ['class' => 'yii\grid\ActionColumn'],
];