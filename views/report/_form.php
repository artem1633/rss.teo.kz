<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Report;
use yii\helpers\Url;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
/* @var $works app\models\Work[] */
/* @var $objects app\models\Object[] */
/* @var $sections app\models\Section[] */
/* @var $photos app\models\Photos[] */
/* @var $newWork app\models\Workcount */


$pluginEvents = [];

$user = Yii::$app->user->identity;
$userRuleAccess = $user->rule;

if ($userRuleAccess == \app\models\User::PERMISSIONS_EMPLOYEE) {
    $pluginEvents = [
        "select2:unselecting" => "function() {}",
        "select2:unselect" => "function() {}",
    ];
}

$css = "
.bootstrap-tagsinput{
    width: 100%;
}

.bootstrap-tagsinput .tag{
    font-size: 12px !important;
}

";

$js = "



$('#btn-works').click(function(e){
    e.preventDefault();
    $('#report-has_works').val(1);
    $('.works').show();
    $(this).hide();
});

$('#works-report_ww').click(function(e){
    e.preventDefault();
    if ($('#report-has_works').val()!=='1'){
        alert('Добавьте работы!');
        return false;
    }
    $('#report-is_send_report').val(1);
    $('#works-form').submit();
});

$('#save_withuot_send').click(function(e){
    e.preventDefault();";
$js .= (!$model->isNewRecord) ? "
    if ($('#report-has_works').val()!=='1'){
        alert('Добавьте работы!');
        return false;
    }" : "";

$js .= "
    $('#report-is_send_report').val(0);
    $('#works-form').submit();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, \"\\$&\");
    var regex = new RegExp(\"[?&]\" + name + \"(=([^&#]*)|&|#|$)\"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, \" \"));
}

setTimeout(function(){
console.log($('.bootstrap-tagsinput input').attr('style', 'width: 10em !important;'));
}, 1000);

";

$this->registerCss($css);
$this->registerJs($js, \yii\web\View::POS_READY);

$project = Yii::$app->request->get('project_id');
if (empty($project)) {
    $project = $model->project_id;
}
if (!empty($model->work)) {

}


if ($model->isNewRecord == true) {
    $reports = Report::find()->all();
} else {
    $reports = $model->findFreeReports();
}

$objects = \app\models\ProjectObject::findAll(['project_id' => $project]);

foreach ($objects as &$item) {
    $item = $item->object;
}
$data = [
    'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
    'options' => ['id' => 'object'],
    'pluginOptions' => [
        'depends' => ['project'],
        'placeholder' => 'Выберите объект...',
        'url' => \yii\helpers\Url::to(['/report/object'])
    ]
];
if (isset($model->object_id)) {
    $data = [
        'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'object'],
        'data' => [$model->object_id => \app\models\Object::findOne($model->object_id)->title],
        'pluginOptions' => [
            'depends' => ['project'],
            'placeholder' => 'Выберите объект...',
            'url' => \yii\helpers\Url::to(['/report/object'])
        ]
    ];
}

?>
<?php if (!empty($model->project_id)) {
    echo "<h3>Ежедневный отчёт " . \app\models\Project::findOne(['id' => $model->project_id])->title . " " . $model->creator->name . "</h3>";
}
?>

    <div class="report-form">
        <div class="box box-default">
            <div class="box-body">
                <?php if (!$model->isNewRecord): ?>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Проект
                                </label>
                                <?= Html::textInput('project_id', $model->project->title,
                                    ['class' => 'form-control', 'disabled' => true]); ?>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Объект
                                </label>
                                <?= Html::textInput('object_id', $model->object->title,
                                    ['class' => 'form-control', 'disabled' => true]); ?>
                            </div>


                        </div>
                        <div class="col-md-4 vcenter">
                            <div class="form-group">
                                <label class="control-label">
                                    Дата проведения работ
                                </label>
                                <?= Html::textInput('begin_work',
                                    (\DateTime::createFromFormat('Y-m-d H:i:s', $model->begin_work)) ?
                                        \DateTime::createFromFormat('Y-m-d H:i:s',
                                            $model->begin_work)->format('d.m.Y') : $model->begin_work
                                    , ['class' => 'form-control', 'disabled' => true]); ?>
                            </div>


                        </div>
                    </div>

                    <div class="works">

                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->render('_add_work', [
                                    'newWork' => $newWork,
                                    'works' => $works,
                                    'objects' => $objects,
                                    'sections' => $sections,
                                    'model' => $model,
                                ]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="work-grid">
                                    <?= $this->render('_work_table', [
                                        'processesDataProvider' => $processesDataProvider,
                                        'workcounts' => $workcounts,
                                        'objects' => $objects,
                                        'works' => $works,
                                        'sections' => $sections,
                                    ]); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php endif; ?>
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'works-form',
                        'enctype' => 'multipart/form-data'
                    ]
                ]); ?>
                <?php if (!$model->isNewRecord): ?>
                    <div class="row" style="display:none">
                        <div class="col-md-4">
                            <?= $form->field($model, 'project_id')->widget(\kartik\select2\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id',
                                    'title'),
                                'options' => ['placeholder' => 'Выберите проект...', 'id' => 'project'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'object_id')->widget(\kartik\depdrop\DepDrop::classname(),
                                $data); ?>
                        </div>
                        <div class="col-md-4 vcenter">

                            <?= $form->field($model, 'begin_work')->widget(DatePicker::classname(), [
                                'language' => 'ru',
                                'dateFormat' => 'php:d.m.Y',
                                'options' => ['class' => 'form-control'],
                            ]); ?>

                        </div>
                    </div>
                <?php endif; ?>
                <?= $form->field($model, 'has_works')->hiddenInput()->label(false); ?>
                <?= $form->field($model, 'is_send_report')->hiddenInput()->label(false); ?>

                <?php if ($model->isNewRecord): ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'project_id')->widget(\kartik\select2\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\app\models\Project::find()->orderBy(['title' => SORT_ASC])->all(),
                                    'id', 'title'),
                                'options' => ['placeholder' => 'Выберите проект...', 'id' => 'project'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'object_id')->widget(\kartik\depdrop\DepDrop::classname(),
                                $data); ?>
                        </div>
                        <div class="col-md-4 vcenter">

                            <?= $form->field($model, 'begin_work')->widget(DatePicker::classname(), [
                                'language' => 'ru',
                                'dateFormat' => 'php:d.m.Y',
                                'options' => ['class' => 'form-control'],
                            ]); ?>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 vcenter">
                            <?= $form->field($model, 'user_list')->label()->widget(\kartik\select2\Select2::classname(),
                                [
                                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'login',
                                        'name'),
                                    'options' => [
                                        'placeholder' => 'Выберите состав бригады...',
                                        'multiple' => true,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                    'showToggleAll' => false,
                                ]); ?>
                        </div>
                        <div class="col-md-6 vcenter">
                            <?= $form->field($model, 'done')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!$model->isNewRecord): ?>


                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'done')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'future_plan')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'problems')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'driving')->textInput() ?>
                        </div>
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'renta_avto')->textInput() ?>
                        </div>
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'avto')->widget(\kartik\select2\Select2::classname(), [
                                'data' => \app\models\Cars::carsList(),
                                'options' => ['placeholder' => 'Выберите автомобиль...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 vcenter">
                            <?= $form->field($model, 'user_list')->label()->widget(\kartik\select2\Select2::classname(),
                                [
                                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'login',
                                        'name'),
                                    'options' => ['placeholder' => 'Выберите состав бригады...', 'multiple' => true,],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                        </div>
                        <div class="col-md-6 vcenter">
                            <?= $form->field($model,
                                'email_dispatch')->label()->widget(\kartik\select2\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'email', 'name'),
                                'options' => ['placeholder' => 'Выберите адреса рассылок...', 'multiple' => true,],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'readonly' => true,
                                ],
                                'pluginEvents' => $pluginEvents,
                            ]); ?>
                        </div>
                        <!--div class="col-md-6 vcenter">
                            <?php /*= $form->field($model, 'prev_report')->widget(\kartik\select2\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map($reports, 'id', 'done'),
                                'options' => ['placeholder' => 'Выберите отчёт...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); */ ?>
                        </div-->
                    </div>

                <?php endif; ?>



                <?php if ($model->isNewRecord == false): ?>
                    <?= $form->field($model, 'imageFiles')->hiddenInput(); ?>
                    <?= \kato\DropZone::widget([
                        'uploadUrl' => Url::toRoute(['report/upload-file']),
                        'options' =>
                            [
                                'maxFilesize' => '5',
                                'addRemoveLinks' => true,
                            ],
                        'clientEvents' => [
                            'success' => 'function(file) {
                                var filePath = file.xhr.responseText.substr(1, file.xhr.responseText.length);
                                var filePath = filePath.substr(0, filePath.length-1);
                                if($("#report-imagefiles").val() == "")
                                {
                                    $("#report-imagefiles").val(filePath);
                                } else {
                                    $("#report-imagefiles").val($("#report-imagefiles").val()+","+filePath);
                                }
                            }',
                        ]
                    ])
                    ?>
                    <div class="photos">

                        <?php \app\assets\MagnificPopupAsset::register($this) ?>
                        <?php foreach ($photos as $photo): ?>
                            <img style="width: 100px; height: 60px; object-fit: cover; margin-top: 10px; cursor: pointer;"
                                 src="http://<?= $_SERVER['SERVER_NAME'] . '/' . $photo->photo ?>"
                                 href="http://<?= $_SERVER['SERVER_NAME'] . '/' . $photo->photo ?>">
                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>
                <div class="form-group" style="margin-top: 10px;">

                    <?php if ($model->isNewRecord): ?>
                        <?= Html::submitButton('Далее', ['id' => 'save_withuot_send', 'class' => 'btn btn-primary']) ?>
                    <?php endif; ?>
                    <?php if (!$model->isNewRecord): ?>
                        <?= Html::submitButton('Отправить отчет',
                            ['id' => 'works-report_ww', 'class' => 'btn btn-primary']) ?>
                    <?php endif; ?>
                    <?php if ($model->isNewRecord == false && isset($model->project_id) && isset($model->object_id)): ?>
                        <?= Html::a('Удалить файлы', 'filedelete?id=' . $model->id, ['class' => 'btn btn-success']) ?>
                    <?php endif; ?>
                </div>

                <?php ActiveForm::end() ?>


                <div style="display:none">
                </div>


            </div>
        </div>
    </div>

<?php

if ($model->isNewRecord == false) {
    $galleryScript = "
	$('.photos').magnificPopup({
		delegate: 'img',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1], // Will preload 0 - before current, and 1 after the current image	
            tPrev: 'Предыдущая', // title for left button
            tNext: 'Следующая', // title for right button
            tCounter: '<span class=\"mfp-counter\">%curr% из %total%</span>' // markup of counter
		},
		image: {
			tError: '<a href=\"%url%\">Изображение #%curr%</a> не может быть загружено.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});
";

    $this->registerJs($galleryScript, \yii\web\View::POS_READY);
}

?>