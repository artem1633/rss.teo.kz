<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $works app\models\Work[] */
/* @var $objects app\models\Object[] */
/* @var $sections app\models\Section[] */
/* @var $photos app\models\Photos[] */
/* @var $newWork app\models\Workcount */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Отчеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="report-update">


    <?= $this->render('_form', [
        'model' => $model,
        'works' => $works,
        'sections' => $sections,
        'newWork' => $newWork,
        'workcounts' => $workcounts,
        'objects' => $objects,
        'photos' => $photos,
        'processesDataProvider' => $processesDataProvider,
    ]) ?>

</div>
