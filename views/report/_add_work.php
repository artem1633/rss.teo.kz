<?php

/* @var $model app\models\Report */
/* @var $works app\models\Work[] */
/* @var $objects app\models\Object[] */
/* @var $sections app\models\Section[] */
/* @var $newWork app\models\Workcount */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$getWorkInfoUrl = 'https://'.$_SERVER['SERVER_NAME'].Url::toRoute(['report/get-amounts']);

$jsOnSelect = "function() {
    var project = $('#work-btn_submit').attr('data-project');
    var object = $('#work-btn_submit').attr('data-object');
    var section = $('#work-section-id').val();
    var work = $(this).val();
   
   $.get('{$getWorkInfoUrl}'+'?project='+project+'&object='+object+'&section='+section+'&work='+work, function(data){
        $('#fw_total').val(data.count);
        $('#fw_unit').val(data.units);
        $('#fw_done').val(data.count_fact);
   });
    
    
}";


$filteredSections = [];

foreach($sections as $section)
{
    if($section->object_id == $model->object_id)
    {
        array_push($filteredSections, $section);
    }
}

\app\assets\ReportCreateAsset::register($this);

?>
<?php $workForm = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'action' => Url::toRoute(['report/add_work', 'project_id' => $model->project_id, 'object_id'  => $model->object_id])]]); ?>


        <div class="col-md-2 work-padding">
            <?= $workForm->field($newWork, 'section')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($filteredSections, 'id', 'title'),
                'options' => ['id' => 'work-section-id', 'placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
//                'pluginEvents' => [
//                    "depdrop:afterChange"=>"function(event, id, value) {
//                        var section = value;
//                        alert(section+' '+work);
//                    }",
//                ],
            ]) ?>
        </div>

        <div class="col-md-2 work-padding">
            <?= $workForm->field($newWork, 'work_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'work-work-id'],
                'pluginOptions' => [
                    'depends' => ['project', 'object', 'work-section-id'],
                    'placeholder' => 'Выберите раздел...',
                    'url' => \yii\helpers\Url::to(['/report/get-section-works']),
                    'loadingText' => 'Поиск работ'
                ],
//                'pluginEvents' => [
//                    "depdrop:afterChange"=>"function(event, id, value) {
//                        var work = value;
//                        alert(section+' '+work);
//                    }",
//                ],
                'select2Options' => [
                    'pluginEvents' => [
                        "select2:select" => $jsOnSelect,
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-md-1 work-padding">
            <label for="">Всего по ВР</label>
            <input id="fw_total" class="form-control" type="text" name="" readonly>
        </div>
        <div class="col-md-1 work-padding">
            <label for="">Ед. изм.</label>
            <input id="fw_unit" class="form-control" type="text" name="" readonly>
        </div>
        <div class="col-md-2 work-padding">
            <label for="">Сделано ранее</label>
            <input id="fw_done" class="form-control" type="text" name="" readonly>
        </div>
        <div class="col-md-2 work-padding">
            <?= $workForm->field($newWork, 'count')->label('Сделано сегодня')->textInput() ?>
        </div>

        <div class="col-md-2 work-padding">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить работу', '#', ['class' => 'btn btn-success', 'data-project' => $model->project_id, 'data-object' => $model->object_id, 'data-url_getinfo' => $getWorkInfoUrl, 'data-report' => Yii::$app->request->get('id'),'data-url' => 'https://'.$_SERVER['SERVER_NAME'].Url::toRoute(['report/add-work']), 'id' => 'work-btn_submit', 'style' => 'margin-top: 24px']); ?>
        </div>



<?php ActiveForm::end(); ?>
</form>
