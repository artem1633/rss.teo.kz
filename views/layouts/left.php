<?php

use \app\models\User;

$user = Yii::$app->user->identity;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN],
                    ['label' => 'Проекты', 'icon' => 'product-hunt', 'url' => ['/project'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN || $user->rule == User::PERMISSIONS_DIRECTOR_PROJECTS],
                    ['label' => 'Объекты', 'icon' => 'object-group', 'url' => ['/object'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN || $user->rule == User::PERMISSIONS_DIRECTOR_PROJECTS || $user->rule == User::PERMISSIONS_PROJECTS_MANAGER],
                    ['label' => 'Работы', 'icon' => 'wrench', 'url' => ['/work'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN || $user->rule == User::PERMISSIONS_PROJECTS_MANAGER || $user->rule == User::PERMISSIONS_DIRECTOR_PROJECTS],
                    ['label' => 'Отчёты', 'icon' => 'envira', 'url' => ['/report']],
                    ['label' => 'Должности', 'icon' => 'user-circle', 'url' => ['/position'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN],
                    ['label' => 'Автомобили', 'icon' => 'car', 'url' => ['/cars'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN || $user->rule == User::PERMISSIONS_PROJECTS_MANAGER || $user->rule == User::PERMISSIONS_DIRECTOR_PROJECTS],
                    ['label' => 'Настройки получения почты', 'icon' => 'cog', 'url' => ['/users/recipients', 'id'=>$user->getId()], 'visible' => $user->rule == User::PERMISSIONS_ADMIN || $user->rule == User::PERMISSIONS_PROJECTS_MANAGER || $user->rule == User::PERMISSIONS_DIRECTOR_PROJECTS,],
                    ['label' => 'Новости', 'icon' => 'paper-plane', 'url' => ['/news'], 'visible' => $user->rule == User::PERMISSIONS_ADMIN],
//                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
//                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
//                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Same tools',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
