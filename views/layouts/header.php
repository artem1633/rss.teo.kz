<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$user = Yii::$app->user->identity;

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Works 3.0</span><span class="logo-lg">Works 3.0</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a> -->
        <a href="#" onclick="$.get('report/mymenu', {'id':1}, function(data){ } );" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span> </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <?= Html::a(
                        '<i class="fa fa-sign-out"></i> Выйти',
                        ['/site/logout'],
                        ['data-method' => 'post']
                    ) ?>


                </li>


            </ul>
        </div>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a title="<?=isset($user->login)?$user->login:""?>" href="#"><?=isset($user->name)?$user->name:""?></a>
                </li>


            </ul>
        </div>


    </nav>
</header>
