<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-index">
    <div class="box box-default">
        <div class="box-body">
            <!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-body" style="overflow-x: auto;">
        <?= GridView::widget([
            'responsiveWrap' => false,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'content' => function ($data) {
                        return '<span class="label label-warning" style="font-size:13px;">' . Html::a($data->id . '   ', ['update', 'id' => $data->id], ['style' => 'color:#ffffff;']) . '</span>';

                    },
                ],
                'title',
                [
                    'label' => 'Проекты',
                    'content' => function ($data) {
                        $projects = \app\models\ProjectObject::findAll(['object_id' => $data->id]);
                        foreach ($projects as &$item)
                        {
                            $project = \app\models\Project::findOne($item->project_id);
                            $item = Html::a($project->title , ['project/update', 'id' => $project->id]);
                        }

                        return implode(',',$projects);
                    }
                ],
                [
                    'label' => 'Разделы',
                    'content' => function ($data) {
                        $projects = \app\models\Section::findAll(['object_id' => $data->id]);
                        foreach ($projects as &$item)
                        {
                            $item = Html::a($item->title , ['section/view', 'id' => $item->id]);
                        }

                        return implode(',',$projects);
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} '],
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
</div>
