<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Object */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Objects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
