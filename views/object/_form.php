<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Object */
/* @var $refe app\models\ProjectObject */
/* @var $form yii\widgets\ActiveForm */

$sections = \app\models\Section::findAll(['object_id' => $model->id]);
$data=[];
foreach ($sections as $item)
{
    $data[$item->id] = $item->title;
}
$data[0] = '';
$reference = new \app\models\Section();


?>

<div class="object-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'section')->widget(\unclead\multipleinput\MultipleInput::className(), [
                        'data' => $data,
                        'allowEmptyList' => false,
                        'enableGuessTitle' => true,
                        'addButtonPosition' => \unclead\multipleinput\MultipleInput::POS_FOOTER // show add button in the header
                    ])->label(false); ?>
                </div>
            </div>
            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
