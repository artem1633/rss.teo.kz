<?php

use yii\db\Migration;

class m170813_092158_add_props extends Migration
{
    public function up()
    {
        $this->addColumn('work','unit',$this->string());
    }

    public function down()
    {
        $this->dropColumn('work','unit');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170813_092158_add_props cannot be reverted.\n";

        return false;
    }
    */
}
