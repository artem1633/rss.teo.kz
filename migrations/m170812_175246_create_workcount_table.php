<?php

use yii\db\Migration;

/**
 * Handles the creation of table `workcount`.
 */
class m170812_175246_create_workcount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('workcount', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'object_id' => $this->integer(),
            'section' => $this->integer(),
            'work_id' => $this->integer(),
            'units'=>$this->string(),
            'count' => $this->integer(),
        ]);

        $this->addColumn('object', 'section', $this->text());
        $this->dropColumn('work', 'count');
        $this->dropColumn('work','units');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('object', 'section');
        $this->dropTable('workcount');
    }
}
