<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report`.
 */
class m170811_180130_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report', [
            'id' => $this->primaryKey(),
            'done'=>$this->text()->notNull(),
            'project_id'=>$this->integer()->notNull(),
            'future_plan'=>$this->text(),
            'problems'=>$this->text(),
            'creator_id'=>$this->integer(),
            'creation_date'=>$this->dateTime(),
            'post_send'=>$this->integer(),
            'prev_report'=>$this->integer(),
        ]);

        $this->createIndex('idx-project-report', 'report', 'project_id', false);
        $this->addForeignKey("fk-project-report", "report", "project_id", "project", "id", 'CASCADE');

        $this->createIndex('idx-user-report', 'report', 'creator_id', false);
        $this->addForeignKey("fk-user-report", "report", "creator_id", "users", "id", 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-report','report');
        $this->dropIndex('idx-project-report','report');

        $this->dropForeignKey('fk-user-report','report');
        $this->dropIndex('idx-user-report','report');

        $this->dropTable('report');
    }
}
