<?php

use yii\db\Migration;

/**
 * Handles the creation of table `section`.
 */
class m170811_180046_create_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('section', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('section');
    }
}
