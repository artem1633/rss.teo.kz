<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object_section`.
 */
class m170811_184023_create_object_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object_section', [
            'id' => $this->primaryKey(),
            'object_id'=>$this->integer()->notNull(),
            'section_id'=>$this->integer()->notNull(),
        ]);

        $this->createIndex('idx-object-section', 'object_section', 'object_id', false);
        $this->addForeignKey("fk-object-section", "object_section", "object_id", "object", "id", 'CASCADE');


        $this->createIndex('idx-section-object', 'object_section', 'section_id', false);
        $this->addForeignKey("fk-section-object", "object_section", "section_id", "section", "id", 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-section-object','object_section');
        $this->dropIndex('idx-section-object','object_section');

        $this->dropTable('object_section');
    }
}
