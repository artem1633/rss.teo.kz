<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photos`.
 */
class m170812_105109_create_photos_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('photos', [
            'id' => $this->primaryKey(),
            'photo'=>$this->string(),
            'report_id'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('photos');
    }
}
