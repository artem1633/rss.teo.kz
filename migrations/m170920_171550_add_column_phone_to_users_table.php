<?php

use yii\db\Migration;

class m170920_171550_add_column_phone_to_users_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('users', 'phone', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('users', 'phone');
    }
}
