<?php

use yii\db\Migration;

class m180129_085831_add_new_fields_to_report extends Migration
{
    public function safeUp()
    {

        $this->addColumn('report','driving',$this->integer()->defaultValue(null));
        $this->addColumn('report','renta_avto',$this->integer()->defaultValue(null));
        $this->addColumn('report','avto',$this->integer()->defaultValue(null));
        $this->addColumn('report','begin_work',$this->dateTime());

    }

    public function safeDown()
    {
        echo "m180129_085831_add_new_fields_to_report cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_085831_add_new_fields_to_report cannot be reverted.\n";

        return false;
    }
    */
}
