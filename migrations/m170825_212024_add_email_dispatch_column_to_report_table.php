<?php

use yii\db\Migration;

/**
 * Handles adding email_dispatch to table `report`.
 */
class m170825_212024_add_email_dispatch_column_to_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report', 'email_dispatch', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report', 'email_dispatch');
    }
}
