<?php

use yii\db\Migration;

/**
 * Handles adding email to table `users`.
 */
class m170826_053249_add_email_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'email', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'email');
    }
}
