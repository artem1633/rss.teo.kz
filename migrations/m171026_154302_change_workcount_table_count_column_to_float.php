<?php

use yii\db\Migration;

class m171026_154302_change_workcount_table_count_column_to_float extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171026_154302_change_workcount_table_count_column_to_float cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->alterColumn('workcount', 'count', $this->float()->defaultValue(0));
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171026_154302_change_workcount_table_count_column_to_float cannot be reverted.\n";

        return false;
    }
    */
}
