<?php

use yii\db\Migration;

/**
 * Handles adding command to table `project`.
 */
class m170910_141728_add_command_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'command', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('project', 'command');
    }
}
