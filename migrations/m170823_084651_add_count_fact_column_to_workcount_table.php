<?php

use yii\db\Migration;

/**
 * Handles adding count_fact to table `workcount`.
 */
class m170823_084651_add_count_fact_column_to_workcount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('workcount', 'count_fact', $this->float()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('workcount', 'count_fact');
    }
}
