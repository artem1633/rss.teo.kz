<?php

use yii\db\Migration;

/**
 * Handles the creation of table `position`.
 */
class m170811_180209_create_position_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('position', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
        ]);

        $this->createIndex('idx-users-role','users','role');
        $this->addForeignKey('fk-position','position','id','users','role');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-position','position');
        $this->dropIndex('idx-users-role','users');

        $this->dropTable('position');
    }
}
