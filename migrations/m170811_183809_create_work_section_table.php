<?php

use yii\db\Migration;

/**
 * Handles the creation of table `work_section`.
 */
class m170811_183809_create_work_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('work_section', [
            'id' => $this->primaryKey(),
            'work_id'=>$this->integer()->notNull(),
            'section_id'=>$this->integer()->notNull(),
        ]);

        $this->createIndex('idx-work-section', 'work_section', 'work_id', false);
        $this->addForeignKey("fk-work-section", "work_section", "work_id", "work", "id");

        $this->createIndex('idx-section-work', 'work_section', 'section_id', false);
        $this->addForeignKey("fk-section-work", "work_section", "section_id", "section", "id");


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-work-section','work_section');
        $this->dropIndex('idx-work-section','work_section');

        $this->dropForeignKey('fk-section-work','work_section');
        $this->dropIndex('idx-section-work','work_section');

        $this->dropTable('work_section');
    }
}
