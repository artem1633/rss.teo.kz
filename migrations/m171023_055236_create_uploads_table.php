<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uploads`.
 */
class m171023_055236_create_uploads_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uploads', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'extension' => $this->string(255),
            'path' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uploads');
    }
}
