<?php

use yii\db\Migration;

class m190328_210141_alter_work_process_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('work_process','count_daily',$this->float());
    }

    public function safeDown()
    {



    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190328_210141_alter_work_process_table cannot be reverted.\n";

        return false;
    }
    */
}
