<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180723_045028_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(   ),
            'content' => $this->text(),
            'date' => $this->dateTime()->defaultValue(date ("Y-m-d H:i:s", time())),
        ]
        );

        $this->insert('news',[
            'title' => 'New Title',
            'content' => 'New content',
            'date' => date ("Y-m-d H:i:s", time())]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
