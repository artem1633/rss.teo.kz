<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m190328_180000_add_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        $this->insert('users',array(
            'name' => 'Admin_rez',
            'role'=>1,
            'rule'=>1,
            'email'=>'admin@gmail.com',
            'login' => 'admin_rez',
            'password' => md5('admin_rez'),
            'is_deleted'=>false,
            'created_at'=>date ("Y-m-d H:i:s", time()),
            'update_at'=>date ("Y-m-d H:i:s", time()),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
