<?php

use yii\db\Migration;

class m180517_070157_add_original_name_column_in_photos_table extends Migration
{
    public function safeUp()
    {
        echo "Добавляем столбец original_name в таблицу photos";

        $this->addColumn('photos', 'original_name', $this->string(255));
    }

    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180517_070157_add_original_name_column_in_photos_table cannot be reverted.\n";

        return false;
    }
    */
}
