<?php

use yii\db\Migration;

class m170815_210339_add_props extends Migration
{
    public function up()
    {
        $this->addColumn('report','object_id',$this->integer());
        $this->addColumn('report','work',$this->string());
    }

    public function down()
    {
       $this->dropColumn('report','object_id');
        $this->dropColumn('report','work');
    }

}
