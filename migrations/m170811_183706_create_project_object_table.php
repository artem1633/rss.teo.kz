<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_object`.
 */
class m170811_183706_create_project_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_object', [
            'id' => $this->primaryKey(),
            'project_id'=>$this->integer()->notNull(),
            'object_id'=>$this->integer()->notNull(),
        ]);

        $this->createIndex('idx-project-object', 'project_object', 'project_id', false);
        $this->addForeignKey("fk-project-object", "project_object", "project_id", "project", "id");


        $this->createIndex('idx-object-project', 'project_object', 'object_id', false);
        $this->addForeignKey("fk-object-project", "project_object", "object_id", "object", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-object','project_object');
        $this->dropIndex('idx-project-object','project_object');
        $this->dropForeignKey('fk-object-project','project_object');
        $this->dropIndex('idx-object-project','project_object');

        $this->dropTable('project_object');
    }
}
