<?php

use yii\db\Migration;

class m180723_020225_set_recipients_column extends Migration
{
    public function safeUp()
    {
        $users = \app\models\User::find()->all();
        $positions = \app\models\Position::find()->all();

        foreach ($positions as &$position){
            $position = $position->id;
        }

        $positions = implode(', ', $positions);

        foreach ($users as $user){
            $user->recipients = $positions;
            $user->save();
        }

    }
}
