<?php

use yii\db\Migration;

class m170812_125338_rm_idx extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-section-object','object_section');
        $this->dropIndex('idx-section-object','object_section');

        $this->dropForeignKey('fk-object-to-section', 'section');
        $this->dropIndex('idx-object-to-section', 'section');

        $this->dropForeignKey('fk-position','position');
        $this->dropIndex('idx-users-role','users');
        $this->dropColumn('users','permission');
        $this->dropColumn('users','is_access');
        $this->dropColumn('users','password_readable');
    }

}
