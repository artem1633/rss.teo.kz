<?php

use yii\db\Migration;

class m170812_072202_add_props_to_section extends Migration
{
    public function up()
    {
        $this->addColumn('section','object_id',$this->integer()->notNull());
        $this->addColumn('work','count',$this->integer());

        $this->dropForeignKey('fk-object-section','object_section');
        $this->dropIndex('idx-object-section','object_section');

        $this->createIndex('idx-object-to-section', 'section', 'object_id', false);
        $this->addForeignKey("fk-object-to-section", "section", "object_id", "object", "id");

        $this->addColumn('report','user_list',$this->string());





    }

    public function down()
    {
        $this->dropForeignKey('fk-object-to-section', 'section');
        $this->dropIndex('idx-object-to-section', 'section');
        $this->dropColumn('section', 'object_id');
        $this->dropColumn('work','count');
        $this->dropColumn('report','user_list');
    }
}
