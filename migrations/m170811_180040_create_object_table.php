<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object`.
 */
class m170811_180040_create_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('object');
    }
}
