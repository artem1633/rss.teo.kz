<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170811_180054_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
            'pm'=>$this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
