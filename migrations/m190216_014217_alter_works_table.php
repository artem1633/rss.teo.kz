<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m190216_014217_alter_works_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('work_process','count_daily',$this->float());

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
