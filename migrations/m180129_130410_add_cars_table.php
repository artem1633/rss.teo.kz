<?php

use yii\db\Migration;

class m180129_130410_add_cars_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('cars', [
            'id' => $this->primaryKey(),
            'name'=>$this->text()->notNull()
        ]);
    }

    public function safeDown()
    {
        echo "m180129_130410_add_cars_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_130410_add_cars_table cannot be reverted.\n";

        return false;
    }
    */
}
