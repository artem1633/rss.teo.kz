<?php

use yii\db\Migration;

/**
 * Handles the creation of table `work`.
 */
class m170811_180153_create_work_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('work', [
            'id' => $this->primaryKey(),
            'title'=>$this->text()->notNull(),
            'units'=>$this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('work');
    }
}
