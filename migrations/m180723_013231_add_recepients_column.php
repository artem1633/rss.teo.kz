<?php

use yii\db\Migration;

class m180723_013231_add_recepients_column extends Migration
{
    public function safeUp()
    {
        echo "Добавляем столбец recipients в таблицу users";

        $this->addColumn('users', 'recipients', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('users','recipients');
    }
}
