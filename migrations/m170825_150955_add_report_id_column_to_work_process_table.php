<?php

use yii\db\Migration;

/**
 * Handles adding report_id to table `work_process`.
 */
class m170825_150955_add_report_id_column_to_work_process_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('work_process', 'report_id', $this->integer()->notNull());

        $this->createIndex(
            'idx-work_process-report_id',
            'work_process',
            'report_id'
        );

        $this->addForeignKey(
            'fk-work_process-report_id',
            'work_process',
            'report_id',
            'report',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-work_process-report_id',
            'work_process'
        );

        $this->dropIndex(
            'idx-work_process-report_id',
            'work_process'
        );

        $this->dropColumn('work_process', 'report_id');
    }
}
