<?php

use yii\db\Migration;

class m170929_073153_alter_report_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('report', 'project_id', $this->integer());
    }

    public function safeDown()
    {
        $this->alterColumn('report', 'project_id', $this->integer()->notNull());
    }
}
