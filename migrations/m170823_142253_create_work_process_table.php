<?php

use yii\db\Migration;

/**
 * Handles the creation of table `work_process`.
 */
class m170823_142253_create_work_process_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('work_process', [
            'id' => $this->primaryKey(),
            'workcount_id' => $this->integer()->notNull(),
            'count_daily' => $this->integer()->notNull(),
            'date' => $this->datetime()
        ]);

        $this->createIndex(
            'idx-work_process-workcount_id',
            'work_process',
            'workcount_id'
        );

        $this->addForeignKey(
            'fk-work_process-workcount_id',
            'work_process',
            'workcount_id',
            'workcount',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-work_process-workcount_id',
            'work_processs'
        );

        $this->dropIndex(
            'idx-work_process-workcount_id',
            'work_process'
        );

        $this->dropTable('work_process');
    }
}
