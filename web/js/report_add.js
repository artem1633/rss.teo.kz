$('#work-btn_submit').click(function(e){
    e.preventDefault();
    var section = $('#work-section-id').val();
    var work = $('#work-work-id').val();
    var count = $('#workcount-count').val();
    var object = $(this).attr('data-object');
    var project = $(this).attr('data-project');
    var url = $(this).attr('data-url');
    var reportId = $(this).attr('data-report');
    var getInfoUrl = $(this).attr('data-url_getinfo');

    if(section == '' || work == '' || count == '')
    {
        alert('Необходимо заполнить все поля для добавления новой работы');
        return;
    }

    var postdata = 'report_id='+reportId+'&project='+project+'&object='+object+'&section='+section+'&work='+work+'&count='+count;

    $.ajax({
        type: "POST",
        url: url,
        data: postdata,
        success: function(data)
        {
            clearFields();
            $.get(getInfoUrl+'?project='+project+'&object='+object+'&section='+section+'&work='+work, function(data){
                $('#fw_total').val(data.count);
                $('#fw_done').val(data.count_fact);
            });
            $('.work-grid').html(data);
            $('#report-has_works').val(1);
        },
        dataType: 'text',
    });

});
$('#workcount-count').keyup(function(e){
    var self = $(this);
    var value = self.val();

    if(IsNumeric(value) == false && value != '')
    {
        self.val(value.substr(0, value.length-1));
        $('#workcount-count').trigger('keyup');
    }
});

function IsNumeric(input)
{
    return (input - 0) == input && (''+input).trim().length > 0;
}

function clearFields()
{
    $('#workcount-section').val('');
    $('#workcount-work_id').val('');
    $('#workcount-count').val('');
}