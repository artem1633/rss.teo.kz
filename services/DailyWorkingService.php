<?php

namespace app\services;

use app\models\Workcount;
use app\models\WorkProcess;

/**
 * Class DailyWorkingService
 * Предназначен для регистрации ежедневных рабочих процессов над работами.
 */
class DailyWorkingService
{

    /**
     * Регистрирует результат за день, создавая новую запись в БД о результате, а также обновляют процесс.
     * @param array $works
     */
    public function registerWorksProcess($works)
    {
        foreach ($works as $workcountId => $value) {
            $workcount = Workcount::findOne($workcountId);
            if ($workcount == null) {
                $workcount = new Workcount();
                $workcount->count = $value;
            } else {
                $workcount->count += $value;
            }

            $workcount->save();

            $process = new WorkProcess(['workcount_id' => $workcountId]);
            $process->count_daily = $value;
            $process->save();
        }
    }
}