<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ReportCreateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/report.css',
    ];
    public $js = [
        'js/report_add.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
        'pudinglabs\tagsinput\TagsInputAsset',
    ];
}
