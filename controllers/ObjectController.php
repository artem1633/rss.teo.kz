<?php

namespace app\controllers;

use app\models\ProjectObject;
use app\models\Section;
use Codeception\Util\Debug;
use Yii;
use app\models\Object;
use app\models\ObjectSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use yii\web\ForbiddenHttpException;

/**
 * ObjectController implements the CRUD actions for Object model.
 */
class ObjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Object models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Object model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Object model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Object();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $post = array_filter(Yii::$app->request->post('Object')['section']);

            $exists = Section::findAll(['object_id' => $model->id]);

            foreach ($exists as $exist)
            {
                $exist->object_id = null;
                $exist->save();
            }

            $exists = ArrayHelper::map($exists, 'id', 'title');

                foreach ($post as $id => $section)
                {
                    if (isset($exists[$id]))
                    {
                        $item = Section::findOne($id);
                    }
                    else
                    {
                        $item = new Section();
                    }

                    $item->title = $section;
                    $item->object_id = $model->id;
                    $item->save();
                }
                return $this->redirect(['index']);
        }
        else
        {
            return $this->render('create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing Object model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $formSections = array_filter(Yii::$app->request->post('Object')['section']);
            $existsSections = ArrayHelper::map(Section::findAll(['object_id' => $model->id]), 'title', 'id');

            foreach ($formSections as $formSection)
            {
                if(isset($existsSections[$formSection]) == false){
                    $section = new Section(['title' => $formSection, 'object_id' => $model->id]);
                    $section->save();
                }
            }

            foreach ($existsSections as $sectionTitle => $sectionId)
            {
                if(in_array($sectionTitle, $formSections) == false){
                    $section = Section::findOne($sectionId);
                    if($section != null){
                        $section->delete();
                    }
                }
            }

            return $this->redirect(['index']);

//            foreach ($exists as $exist)
//            {
//                $exist->object_id = null;
//                $exist->save();
//            }
//
//            $exists = ArrayHelper::map($exists, 'id', 'title');
//
//
//            if (is_array($sections))
//            {
//                foreach ($sections as $id => $section)
//                {
//                    if (isset($exists[$id]))
//                    {
//                        $item = Section::findOne($id);
//                    }
//                    else
//                    {
//                        $item = new Section();
//                    }
//
//                    $item->title = $section;
//                    $item->object_id = $model->id;
//                    $item->save();
//                }
//                return $this->redirect(['index']);
//            }
        }
        else
        {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Object model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public
    function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Object model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Object the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = Object::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        if($user->rule != User::PERMISSIONS_ADMIN && $user->rule != User::PERMISSIONS_DIRECTOR_PROJECTS && $user->rule != User::PERMISSIONS_PROJECTS_MANAGER){
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return parent::beforeAction($action);
    }

    public
    function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }
}
