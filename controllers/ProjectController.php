<?php

namespace app\controllers;

use app\models\ProjectObject;
use app\models\User;
use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Uploads;
use app\models\Work;
use app\models\Object;
use app\models\Section;
use app\models\Workcount;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpload($id)
    {
       $model = new Uploads();
       $model->proyekt_id = $id;
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post() )) {
            $model->File = UploadedFile::getInstance($model, 'File');
            
            if ($model->upload()) {
                return $this->redirect(['getexcel', 'id' => $model->proyekt_id]);            
            }
        }

       //$model->proyekt_id = $id;
       return $this->render('form', ['model' => $model]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        $button = 0;
        //if(isset($_POST['button1']))echo "birinchisi";
        if(isset($_POST['button2']))$button = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $alreadySelected = ProjectObject::findAll(['project_id' => $model->id]);

            $selected = [];

            foreach ($alreadySelected as $items)
            {
                $selected[] = $items->object->id;
            }

            foreach ($alreadySelected as $item)
            {
                $item->delete();
            }

            $post = Yii::$app->request->post('ProjectObject');

            if (is_array($post['object_id']))
            {
                $post = $post['object_id'];

                foreach ($post as $key => $item)
                {
                    $projectObject = new ProjectObject();
                    $projectObject->project_id = $model->id;
                    $projectObject->object_id = $item;
                    $projectObject->save();
                }
            }
            if($button == 1) return $this->redirect(['upload','id'=>$model->id]);
            else return $this->redirect(['update','id'=>$model->id]);
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $alreadySelected = ProjectObject::findAll(['project_id' => $model->id]);

            $selected = [];

            foreach ($alreadySelected as $items)
            {
                $selected[] = $items->object->id;
            }

            foreach ($alreadySelected as $item)
            {
                $item->delete();
            }

            $post = Yii::$app->request->post('ProjectObject');
/*echo "<pre>";
print_r(Yii::$app->request->post());
die;*/
            if (is_array($post['object_id']))
            {
                $post = $post['object_id'];

                foreach ($post as $key => $item)
                {
                    $projectObject = new ProjectObject();
                    $projectObject->project_id = $model->id;
                    $projectObject->object_id = $item;
                    $projectObject->save();
                }
            }
            return $this->redirect(['update','id'=>$model->id]);
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function GetExcelValues()
    {
        $inputFile = 'upload/excelfileimport.xlsx';        
        try{
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        }
        catch(Exception $e)  {  die('Error');  }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $data = []; $error = '<b style="color: #ff0000;"> <i>Нет сведения </i></b>';$validatsiya = 1;
        
        for($row = 2; $row <= $highestRow; $row++)
        {
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
           /* echo "w=".$rowData[0][0];
              echo "<pre>";
              print_r($rowData);
              echo "</pre>";
              die;*/
                                                
            if($row == 1)   {continue; }
            if($rowData[0][0]==""|$rowData[0][1]==""|$rowData[0][2] ==""|$rowData[0][3]==""|$rowData[0][4]==""|$rowData[0][5]=="")$validatsiya = 0;
            $data [] = [
                'proyekt' => $rowData[0][0] == "" ? $error : $rowData[0][0],
                'obyekt' => $rowData[0][1] == "" ? $error : $rowData[0][1],
                'razdel' => $rowData[0][2] == "" ? $error : $rowData[0][2],
                'name' => $rowData[0][3] == "" ? $error : $rowData[0][3],
                'izmereniya' => $rowData[0][4] == "" ? $error : $rowData[0][4],
                'count' => $rowData[0][5] == "" ? $error : $rowData[0][5],
            ]; 
        }
        $result [] = [
            'data' => $data,
            'validatsiya' => $validatsiya, 
        ];
        return $result;
    }
    public function Write_to_db($data,$id)
    {  /* echo "id=".$id;
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;*/
        foreach ($data as $value) {
            //echo "f=".$value['obyekt'];die;
            $object = Object::find()->where([ 'title' => $value['obyekt'] ])->one();
            if(isset($object)){ $object_id = $object->id; }
            else {
                $model = new Object();
                $model->title = $value['obyekt'];
                $model->save();
                $object_id = $model->id;
            }

            $project_object = ProjectObject::find()->where(['project_id' => $id, 'object_id' => $object_id])->one();
            if(!isset($project_object)){ 
                $model = new ProjectObject();
                $model->project_id = $id;
                $model->object_id = $object_id;
                $model->save();
            }

            $section = Section::find()->where(['title' => $value['razdel'], 'object_id' => $object_id])->one();
            if(!isset($section)){ 
                $model = new Section();
                $model->title = $value['razdel'];
                $model->object_id = $object_id;
                $model->save();
                $section_id = $model->id;
            }
            else { $section_id = $section->id; }

            $work = Work::find()->where(['title' => $value['name'], 'unit' => $value['izmereniya']])->one();
            if(!isset($work)){ 
                $model = new Work();
                $model->title = $value['name'];
                $model->unit = $value['izmereniya'];
                $model->save();
                $work_id = $model->id;
            }
            else { $work_id = $work->id; }

            $work_count = new Workcount();
            $work_count->project_id = $id;
            $work_count->object_id = $object_id;
            $work_count->section = $section_id;
            $work_count->work_id = $work_id;
            $work_count->units = $value['izmereniya'];
            $work_count->count = $value['count'];
            $work_count->count_fact = 0.0;
            $work_count->save();
            /*if( $work_count->save() )echo "saqlandi";
            else echo "not";
           
              */
             /*echo "<pre>";
            print_r( $work_count->errors);
            echo "</pre>";
            die; */
            /*echo "<pre>";
        print_r($work_count);
        echo "</pre>";
        die;*/
        }
    }

    public function SortingValues($data,$id)
    {
        $allworks = \app\models\Workcount::find()->where(['project_id' => $id])->all();

        $works = [];
        foreach ($allworks as $value) {
           $works [] =[
                'proyekt' => Project::find()->where(['id' => $value['project_id']])->one()->title,
                'obyekt' => Object::find()->where(['id' => $value['object_id']])->one()->title,
                'razdel' => Section::find()->where(['id' => $value['section']])->one()->title,
                'name' =>  Work::find()->where(['id' => $value['work_id']])->one()->title,
                'izmereniya' => $value['units'],
                'count' => $value['count'],
           ]; 
        }
        
        $result = [];
        if($works != null){
            foreach ($data as $d) {
                $q = 1;
               foreach ($works as $w) {
                   if($w['proyekt'] != $d['proyekt']) {  $q = 0; break;}
                   if($w['obyekt'] == $d['obyekt'] && $w['razdel'] == $d['razdel'] && $w['name'] == $d['name'] && $w['izmereniya'] == $d['izmereniya'] && $w['count'] == $d['count']) { $q = 0; break;}
                }
                if($q == 1)  $result [] = $d;
            }
        }
        else { 
           $proyekt = Project::findOne($id);
           foreach ($data as $d) {
                if($proyekt->title == $d['proyekt']) $result [] = $d;
           } 
        }

        /*echo "<pre>";
        print_r($result);
        echo "<pre>";
        die;
        print_r($result);*/
        return $result;
    }

    public function actionGetexcel($id, $testing = null)
    {
        $model = $this->findModel($id);
        $excel_data = $this->GetExcelValues();
        $validatsiya = $excel_data[0]['validatsiya'];
        $excel_data = $excel_data[0]['data'];
            /*echo "<pre>";
            print_r($validatsiya);
            echo "</pre>";
            die;*/

        if($validatsiya == 0) {
            \Yii::$app->session->setFlash('error', 'В выбранном файле заполнены не все сведения. Откорректируйте и загрузите файл заново');
            return $this->render('read_excel', [
                'model' => $model,
                'excel_data' => $excel_data,
                'retry' => 1,
            ]);
        }

        if($testing == null) {
            $excel_data = $this->SortingValues($excel_data,$id);
            return $this->render('read_excel', [
                'model' => $model,
                'excel_data' => $excel_data,
                'retry' => null,
            ]);
        }

        if($testing == 1) {

            $excel_data = $this->SortingValues($excel_data,$id);
            return $this->render('read_excel', [
                'model' => $model,
                'excel_data' => $excel_data,
                'retry' => null,
            ]);
        }

        if($testing == 2) {

            $excel_data = $this->SortingValues($excel_data,$id);
            $result = $this->Write_to_db($excel_data,$id);

            /*return $this->render('read_excel', [
                'model' => $model,
                'excel_data' => $excel_data,
            ]);*/
            return $this->redirect(['update','id'=>$id]);
        }


/*                if ($model->load(Yii::$app->request->post()) && $model->save())
                {
                    $alreadySelected = ProjectObject::findAll(['project_id' => $model->id]);

                    $selected = [];

                    foreach ($alreadySelected as $items)
                    {
                        $selected[] = $items->object->id;
                    }

                    foreach ($alreadySelected as $item)
                    {
                        $item->delete();
                    }

                    $post = Yii::$app->request->post('ProjectObject');

                    if (is_array($post['object_id']))
                    {
                        $post = $post['object_id'];

                        foreach ($post as $key => $item)
                        {
                            $projectObject = new ProjectObject();
                            $projectObject->project_id = $model->id;
                            $projectObject->object_id = $item;
                            $projectObject->save();
                        }
                    }
                    return $this->redirect(['read_excel','id'=>$model->id]);
                }
                else
                {
                    return $this->render('read_excel', [
                        'model' => $model,
                    ]);
                }*/
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        if($user->rule != User::PERMISSIONS_ADMIN && $user->rule != User::PERMISSIONS_DIRECTOR_PROJECTS){
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return parent::beforeAction($action);
    }

    public function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }

}
