<?php

namespace app\controllers;

use app\models\Cars;
use app\models\Object;
use app\models\Position;
use app\models\Project;
use app\models\ProjectObject;
use app\models\Section;
use app\models\UploadForm;
use app\models\User;
use app\models\Users;
use app\models\Work;
use app\models\Workcount;
use app\models\WorkProcess;
use app\models\WorkProcessSearch;
use Yii;
use app\models\Report;
use app\models\ReportSearch;
use app\models\Photos;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use app\services\DailyWorkingService;
use yii\data\ActiveDataProvider;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderExel = $searchModel->searchExel(Yii::$app->request->queryParams);
        $users = Users::find()->all();

        if (!is_dir('uploads')) {
            mkdir('uploads');
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderExel' => $dataProviderExel,
            'usersMap' => ArrayHelper::map($users, 'id', 'name'),
            'usersMapByLogins' => ArrayHelper::map($users, 'login', 'name'),
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     */
    /* public function actionView($id)
     {
         return $this->render('view', [
             'model' => $this->findModel($id),
         ]);
     }*/

    /**
     * Экшен создания нового отчета
     * Если пришел $original_id тогда создаем новый отчет основываясь на отчете, id которого пришел
     *
     * @param null $original_id
     * @return mixed
     */
    public function actionCreate($original_id = null)
    {

        //если мы создаем через копирование, то заполняем из оригинала
        if ($original_id) {
            $model = $this->getCopyTemplate($original_id);
        } else {
            $model = new Report();
        }

        $newWork = new Workcount();
        $objects = Object::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $model->work = Yii::$app->request->post('work');

            $workcounts = Workcount::find()->where([
                'project_id' => $model->project_id,
                'object_id' => $model->object_id
            ])->all();

            $works = Work::findAll($this->filterByWorkcountWorks($workcounts));
            $sections = Section::findAll($this->filterByWorkcountSections($workcounts));

            if (!is_dir('uploads')) {
                mkdir('uploads');
            }
            if (Yii::$app->request->isPost) {
                $newModel = new UploadForm();
                $newModel->imageFiles = UploadedFile::getInstances($model, 'imageFiles');

                $newModel->upload($model->id);
            }
            $model->save();

            if ($model->isSentFlag) {
                return $this->redirect(['report/index']);
            } else {
                return $this->redirect(['report/update', 'id' => $model->id, 'sw' => 1]);
            }
        } else {

            //При создании нового отчета в бригаду уже должен быть включен сотрудник создающий отчет
            //при копировании в этом нет необходимости
            if (!$original_id) {
                /** @var User $identity */
                $identity = Yii::$app->user->identity;
                $model->user_list = [$identity->id => $identity->login];
            }

            $works = [];
            $sections = [];
            $workcounts = [];
            $processesDataProvider = null;

            return $this->render('create', [
                'model' => $model,
                'works' => $works,
                'sections' => $sections,
                'newWork' => $newWork,
                'workcounts' => $workcounts,
                'objects' => $objects,
                'processesDataProvider' => $processesDataProvider
            ]);
        }
    }


    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $newWork = new Workcount();
        $objects = Object::find()->all();
        if (!is_dir('uploads')) {
            mkdir('uploads');
        }
        $model = $this->findModel($id);
        $photos = Photos::find()->where(['report_id' => $model->id])->all();
        $emails = Users::find()->where([
            'in',
            'login',
            Project::findOne(['id' => $model->project_id])->command
        ])->all();

        $users = [];

        foreach ($emails as $user) {
            $users[] = $user->email;
        }

        foreach ($model->user_list as $userLogin) {
            $userSearch = Users::findOne(['login'=>$userLogin]);
            $users[] = $userSearch->email;
        }

        $model->email_dispatch = array_merge($users,$model->user_list);

        $workcounts = Workcount::find()->where([
            'project_id' => $model->project_id,
            'object_id' => $model->object_id
        ])->all();
        $processesSearch = new WorkProcessSearch();
        $processesDataProvider = $processesSearch->searchByWorkcounts($id, array_column($workcounts, 'id'));
        if ($processesDataProvider->totalCount) {
            $model->has_works = 1;
        } else {
            $model->has_works = 0;
        }
        $works = Work::findAll($this->filterByWorkcountWorks($workcounts));
        $sections = Section::findAll($this->filterByWorkcountSections($workcounts));


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->work = Yii::$app->request->post('work');

            if (!empty($model->work)) {
                $model->work = json_encode($model->work);
                $work = json_decode($model->work);
                $workingService = new DailyWorkingService();
                $workingService->registerWorksProcess($work);
            }

            if (Yii::$app->request->isPost) {
                $newModel = new UploadForm();
                $newModel->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                $newModel->upload($model->id);
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'works' => $works,
                'sections' => $sections,
                'newWork' => $newWork,
                'workcounts' => $workcounts,
                'objects' => $objects,
                'photos' => $photos,
                'processesDataProvider' => $processesDataProvider
            ]);
        }
    }

    public function actionGetAmounts($project, $object, $section, $work)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $work = Workcount::findOne([
            'project_id' => $project,
            'object_id' => $object,
            'section' => $section,
            'work_id' => $work
        ]);

        if ($work == null) {
            throw new NotFoundHttpException('Работа не найдена');
        }

        return $work;
    }

    public function actionDeleteProcess($id, $report_id)
    {
        $process = WorkProcess::findOne($id);
        if ($process != null) {
            $process->delete();
            return $this->redirect(['report/update', 'id' => $report_id]);
        } else {
            throw new NotFoundHttpException('Объект не найден');
        }
    }

    public function actionAddWork()
    {
        $post = Yii::$app->request->post();
        $section_id = intval($post['section']);
        $work = intval($post['work']);
        $count = floatval($post['count']);
        $project = intval($post['project']);
        $object = intval($post['object']);
        $report_id = intval($post['report_id']);

        $sections = Section::find()->all();
        $works = Work::find()->all();
        $objects = Object::find()->all();

        if ($section_id == null || $work == null || $count == null || $object == null || $project == null) {
            throw new HttpException(400, 'Не хватает нужных параметров');
        }

        $workCount = Workcount::find()->where([
            'section' => $section_id,
            'object_id' => $object,
            'project_id' => $project,
            'work_id' => $work
        ])->one();
        $workCount->work_id = strval($workCount->work_id);
        $workCount->count_fact = $workCount->count_fact + $count;
        $workCount->save();

        $workProcess = new WorkProcess([
            'report_id' => $report_id,
            'workcount_id' => $workCount->id,
            'count_daily' => $count,
        ]);
        $workProcess->save();

        $workCounts = Workcount::find()->where(['project_id' => $project, 'object_id' => $object])->all();

        $processesSearch = new WorkProcessSearch();
        $processesDataProvider = $processesSearch->searchByWorkcounts($report_id, array_column($workCounts, 'id'));

        $table = $this->renderPartial('_work_table', [
            'processesDataProvider' => $processesDataProvider,
            'workcounts' => $workCounts,
            'objects' => $objects,
            'works' => $works,
            'sections' => $sections,
            'report_id' => $report_id,
        ]);

        return $table;

    }

    public function actionViewInModal($id)
    {
        $model = $this->findModel($id);
        return $this->renderPartial('_view_modal', [
            'model' => $model,
        ]);
    }

    public function actionFiledelete($id, $photo = null)
    {
        /*$photos = Photos::find()->where(['report_id' => $model->id])->all();*/
        //$post = Yii::$app->request->post();
        /*echo "<pre>";
        print_r($photo);
        echo "</pre>";*/
        if ($photo != null) {
            $file = Photos::findOne($photo);
            $file->delete();
        }

        $photos = Photos::find()->where(['report_id' => $id])->all();
        return $this->render('delete_file', [
            'photos' => $photos,
            'report_id' => $id,
        ]);
    }

    public function actionUploadFile()
    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            $fileName = Yii::$app->security->generateRandomString();

            //ориинальное имя файла
            $original_name = $file->name;

            //полный путь по которому реально хранится файл
            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;

            //если получилось сохранить файл
            if ($file->saveAs($path)) {
                //создаем запись в талице photos с 0 вместо report_id отчета
                if (Photos::createTempPhoto($path, $original_name)) {
                    //отдаем ссылку на файл в виев
                    echo \yii\helpers\Json::encode($path);
                }
            }
        }

        return false;
    }

    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Проверяет соответствие правилам доступа и отдает рекорд
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = ReportSearch::findModel($id);
        $this->checkRules($model);
        return $model;
    }


    /**
     * Отдает заполненный рекорд для создания
     *
     * По хорошему перенести метод на сервисный слой
     *
     * @param $id
     * @return Report
     */
    private function getCopyTemplate($id)
    {
        $original = $this->findModel($id);

        $template = new Report();

        //заполняем данные, которые должны быть заполнены при копировании
        $template->project_id = $original->project_id;
        $template->object_id = $original->object_id;
        $template->user_list = $original->user_list;
        $template->creator_id = $original->creator_id;
        $template->done = $original->done;
        $template->future_plan = $original->future_plan;
        $template->problems = $original->problems;
        $template->email_dispatch = $original->email_dispatch =='' ? [] : explode(',', $original->email_dispatch[0]);

        return $template;
    }


    protected function checkRules($model)
    {
        $user = Yii::$app->user->identity;
        if ($user->rule == User::PERMISSIONS_EMPLOYEE) {
            if ($model->creator_id != $user->id) {
                throw new ForbiddenHttpException('Нету прав доступа к данному отчету.');
            }
        }
        if ($user->rule == User::PERMISSIONS_PROJECTS_MANAGER) {
            if ($model->project->pm == $user->id) {
                return true;
            }
            if ($model->creator_id != $user->id) {
                throw new ForbiddenHttpException('Нету прав доступа к данному отчету.');
            }

        }
    }

    /**
     * @param \app\models\Workcount[] $workcounts
     * @return integer[]
     */
    private function filterByWorkcountWorks($workcounts)
    {
        $worksIds = [];
        foreach ($workcounts as $workcount) {
            array_push($worksIds, $workcount->work_id);
        }

        return array_unique($worksIds);
    }

    /**
     * @param \app\models\Workcount[] $workcounts
     * @return integer[]
     */
    private function filterByWorkcountSections($workcounts)
    {
        $sectionsIds = [];
        foreach ($workcounts as $workcount) {
            array_push($sectionsIds, $workcount->section);
        }

        return array_unique($sectionsIds);
    }

    public function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }

    public function actionObject()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = ProjectObject::findAll(['project_id' => $cat_id]);
                $result = [];
                foreach ($out as &$item) {
                    $item = $item->object;
                    $result[] = ['id' => $item->id, 'name' => $item->title];

                }

                $out = $result;

                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionMymenu($id)
    {
        $session = Yii::$app->session;
        $menu = isset($_SESSION['menu']) ? $_SESSION['menu'] : null;
        if ($menu == null) {
            $_SESSION['menu'] = 'large';
        } else {
            if ($menu == 'large') {
                $_SESSION['menu'] = 'small';
            } else {
                $_SESSION['menu'] = 'large';
            }
        }
    }

    public function actionDaily()
    {
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("RSS");

        $searchModel = new ReportSearch();

        if(isset(Yii::$app->request->queryParams[1])) {
            $dataProvider = $searchModel->searchExel(Yii::$app->request->queryParams[1]);
        }else{
            $dataProvider = $searchModel->searchExel(Yii::$app->request->queryParams);
        }

        //Заголовки
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Пользователь');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Дата работ');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'ФИО');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Должность');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Проект');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Объект');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'Раздел');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'Работа');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', 'Кол-во');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', 'Ед.изм.');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', 'Водительские');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', 'Аренда машин');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', '% оплаты');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', 'Ставка');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', 'Сумма сдельн');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', 'Сумма фикс');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', 'Дорожные');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1', 'Сделано за день');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1', 'Состав бригады');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1', 'Авто');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1', 'План на следующий день');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1', 'Проблемы');


//                'content' => function ($data) use ($usersMapByLogins) {
//                    if ($users = explode(', ', $data['report_user_list'])) {
//                        $userNames = [];
//                        foreach ($users as $user) {
//                            $userNames[] = isset($usersMapByLogins[$user]) ? $usersMapByLogins[$user] : "Пользователь не найден";
//                        }
//                        return implode(", ", $userNames);
//                    }
//                    return null;
        $users = Users::find()->all();
        $usersMapByLogins = ArrayHelper::map($users, 'login', 'name');
        $cars = ArrayHelper::map(Cars::find()->all(),'id','name');
        $positions = ArrayHelper::map(Position::find()->all(),'id','title');
        $index =2; $C = '$C'; $H = '$H';  $B = '$B'; $Q = '$Q';
        foreach ($dataProvider->getModels() as $item) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$index, $item['user_login']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$index, isset($item['report_begin_work']) ?
                \DateTime::createFromFormat('Y-m-d H:i:s', $item['report_begin_work'])->format('d.m.Y') : false);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$index, $item['user_name']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$index, $item['user_role'] ? $positions[$item['user_role']] : null);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$index, $item['project_name']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$index, $item['object_name']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$index, $item['section_name']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$index, $item['work_name']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$index, $item['work_count']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$index, $item['work_units']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$index, $item['report_driving']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$index, $item['report_renta_avto']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$index, '100');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$index, "ВПР(C$index;'[Оклады.xlsx]Лист 1'!$C$1:$H$500;4;ЛОЖЬ)");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$index, "=I$index*N$index*M$index/100");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$index, '"ВПР(C" . $iindex . ";\'Сводная ведомость\'!$B$1:$Q$300;16;ЛОЖЬ)*O" .
                 $index . "/ВПР(C" . $iindex . ";\Сводная ведомость\'!$B$1:$Q$300;15;ЛОЖЬ)"');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$index, "=K$index+L$index*2");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$index, stripslashes(nl2br($item['report_done'])));
                if ($users = explode(', ', $item['report_user_list'])) {
                    $brigada =[];
                        foreach ($users as $user) {
                             $brigada[] = isset($usersMapByLogins[$user]) ? $usersMapByLogins[$user] : "Пользователь не найден";
                        }
                        if (!empty($brigada)) {
                            $username = implode(", ", $brigada);
                        } else {
                            $username = null;
                        }  

                    } else {
                    $username = null;
                }
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$index, $username);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$index, $item['auto'] ? $cars[$item['auto']] : null);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$index, $item['future_plan']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$index, $item['problems']);
            $index++;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ежедневный_отчёт-' . date('d.m.Y') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function actionStats()
    {
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("RSS");

        $searchModel = new ReportSearch();

        if(isset(Yii::$app->request->queryParams[1])) {
            $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams[1]);
        }else{
            $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);
        }
        
        $dates = [];

        $type = false;

        if (Yii::$app->request->queryParams) {
            if (isset(Yii::$app->request->queryParams['ReportSearch']['creation_date']) && !empty(Yii::$app->request->queryParams['ReportSearch']['creation_date'])) {
                $dates = Yii::$app->request->queryParams['ReportSearch']['creation_date'];
                $datesArr = explode(' - ', $dates);
                $dateTo = $datesArr[1];
                $dateFrom = $datesArr[0];
                $type = 'creation_date';
                $textType = ' (Использован фильтр "Дата создания")';

                $dates = $this->createDateRangeArray($datesArr[0], $datesArr[1]);
            } elseif (isset(Yii::$app->request->queryParams['ReportSearch']['begin_work']) && !empty(Yii::$app->request->queryParams['ReportSearch']['begin_work'])) {
                $dates = Yii::$app->request->queryParams['ReportSearch']['begin_work'];
                $type = 'begin_work';
                $datesArr = explode(' - ', $dates);
                $dateTo = $datesArr[1];
                $dateFrom = $datesArr[0];
                $textType = ' (Использован фильтр "Дата работ")';

                $dates = $this->createDateRangeArray($datesArr[0], $datesArr[1]);
            }else {
                $dateNow = new \DateTime('now');
                $textType = ' (Использован фильтр "Дата создания")';
                $dateTo = $dateNow->format("Y-m-d");
                $dateFrom = $dateNow->modify('-1 month')->format("Y-m-d");

                $dates = $this->createDateRangeArray($dateFrom, $dateTo);
            }
        } else {
            $dateNow = new \DateTime('now');
            $textType = ' (Использован фильтр "Дата создания")';
            $dateTo = $dateNow->format("Y-m-d");
            $dateFrom = $dateNow->modify('-1 month')->format("Y-m-d");

            $dates = $this->createDateRangeArray($dateFrom, $dateTo);
        }

        $dataHeader = [
            'fio' => 'Фио',
            'position' => 'Должность',
        ];

        foreach ($dates as $date) {
            $dataHeader[$date] = $date;
        }

        array_push($dataHeader, 'Всего');

        $userReports = [];


        foreach ($dataProvider->getModels() as $item) {
            if($type == 'begin_work') {
                $creationDay = explode(' ', $item->begin_work)[0];
            }else{
                $creationDay = explode(' ', $item->creation_date)[0];
            }

            $userReports[$item->creator_id][] = $creationDay;
        }

        $reportData = [];

        $startIndex = 3;

        foreach ($userReports as $userId => $userReport) {

            $user = User::findOne($userId);

            $days = 1;
            $reportData[$startIndex]['fio'] = $user->name;
            $reportData[$startIndex]['position'] = $user->getPosition()->one()->title;

            foreach ($dates as $date) {
                $reportData[$startIndex][$date] = '';
            }


            foreach ($userReport as $report) {
                $report = $this->getDayMonthFromString($report);

                if (isset($reportData[$startIndex][$report])) {
                    $reportData[$startIndex][$report] = (int)$reportData[$startIndex][$report] + 1;
                }
            }

            $allValues = 0;

            foreach ($reportData[$startIndex] as $item) {
                $allValues += (int)$item;
            }


            $reportData[$startIndex]['Всего'] = $allValues;

            $startIndex++;
        }

        $objPHPExcel->createSheet(0);

        foreach ($reportData as $index => $array) {
            $startCol = 'A';

            foreach ($array as $item) {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($startCol . $index, $item);
                $lastCol = $startCol;
                $startCol++;
            }
        }

        foreach ($dataHeader as &$item) {

            $data = explode('-', $item);

            if (count($data) > 1) {
                $item = (string)$data['1'];
            } else {
                $item = (string)$data['0'];
            }
        }

        $lastCol = '';
        $startCol = 'A';
        foreach ($dataHeader as $headerItem) {
            $lastCol = $startCol;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($startCol . 2, $headerItem);
            $startCol++;
        }

        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                )
            ),
            'font' => array(
                'bold' => true
            )
        );

        $style1 = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                )
            ),
        );

        $rowsCount = count($reportData) + 2;

        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:" . $lastCol . '1')->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A2:" . $lastCol . '2')->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle($lastCol . "0:" . $lastCol . $rowsCount)->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $dateFrom . ' - ' . $dateTo . $textType);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:" . $lastCol . '1');
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A" . $rowsCount . ":" . $lastCol . $rowsCount)->applyFromArray($style1);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="еженедельный_отчёт-' . date('d.m.Y') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function actionGetSectionWorks()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $project_id = $parents[0];
                $object_id = $parents[1];
                $section_id = $parents[2];
                $workCounts = Workcount::find()->where([
                    'project_id' => $project_id,
                    'object_id' => $object_id,
                    'section' => $section_id
                ])->all();
                $workIds = array_column($workCounts, 'work_id');
                $out = Work::findAll($workIds);
                $result = [];
                foreach ($out as &$item) {
                    $result[] = ['id' => $item->id, 'name' => $item->title];
                }

                $out = $result;

                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    private function getDayFromString($string)
    {
        if(is_array($string)){
            $string = $string[0];
        }

        preg_match("!-[0-9]{2}!", $string, $match);

        return str_replace('-', '', $match[0]);
    }

    private function getDayMonthFromString($string)
    {
        preg_match("!-[0-9]{2}-[0-9]{2}!", $string, $match);

        $string =  substr_replace($match,'',0,1);
        return $string[0];
    }

    private function createDateRangeArray($strDateFrom, $strDateTo)
    {
        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2),
            substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('m-d', $iDateFrom));
            }
        }

        return $aryRange;
    }
}
